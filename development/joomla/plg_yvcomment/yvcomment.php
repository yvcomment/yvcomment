<?php

/**
 * yvComment plugin, part of yvComment - the first native Joomla! 1.5 Commenting Solution
 * For additional Comment Type (e.g. N ) you should make changes in this code
 *  (and in the accompanying .xml file) as noted below. 
 * @version		$Id: yvcomment.php 33 2013-02-09 15:18:41Z yvolk $
 * @package yvCommentPlugin
 * @(c) 2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');

if (!class_exists('yvCommentHelper')) {
	$path = JPATH_SITE . DS . 'components' . DS . 'com_yvcomment' . DS . 'helpers.php';
	if (file_exists($path)) {
		require_once ($path);
	}
}
if (!class_exists('yvCommentHelper')) {
	// Create dummy class so the code below will compile!
	$str1 = 'class yvCommentPlugin {' 
	. ' protected $_PluginPlace = "(was not set)";'
	. ' public function __construct($subject, $config = array()){ }' 
	. '	}';
	eval($str1);	
}

// For Comment TypeN append N to the Class name: plgContentyvcommentN
class plgContentyvcomment extends yvCommentPlugin {
	// For Comment TypeN change the value to N instead of 1
	public static $CommentTypeIdStatic = 1;
	// Main Release Level. Extensions for the same Release are compatible
	private static $_Release = '2.01';
	// For Comment TypeN append N to the ExtensionName
	private static $_ExtensionName = 'yvCommentPlugin';
	
	public static function initializeMe() {
		$Ok = false;
		if (class_exists('yvCommentHelper')) {
			if (strtoupper( $_SERVER['REQUEST_METHOD'] ) == 'HEAD') {
				// hide, cause something works wrong in this case
			} else {
				$Ok = yvCommentHelper::VersionChecks(self::$_ExtensionName, self::$_Release);
			}
			if ($Ok) {
				yvCommentHelper::$ContentPluginsImported = true;
				// Import library dependencies
				jimport('joomla.event.plugin');
				self::$hide = false;
			}
		} else {
			// No yvComment Component
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage(
			self::$_ExtensionName . 
				' is installed, but &quot;<b>yvComment Component</b>&quot;' .
				' is <b>not</b> installed. Please install it to use' .
				' <a href="http://yurivolkov.com/Joomla/yvComment/index_en.html" target="_blank">yvComment solution</a>.<br/>' . 
				'(yvComment Plugin version="' . self::$_Release . '")',
  	'error');
				
		}
	}

	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		$config['comment_type_id'] = self::$CommentTypeIdStatic;
		parent::__construct($subject, $config);

		$debug = false;
		if ($debug) {
			echo 'plg_yvcomment' .
			( self::$CommentTypeIdStatic == 1 ? '' : self::$CommentTypeIdStatic) .
			'; position="' . $this->_PluginPlace . '"<br/>';
		}
	}
}

// For Comment TypeN append N to the Class name: plgContentyvcommentN
plgContentyvcomment::initializeMe();

?>