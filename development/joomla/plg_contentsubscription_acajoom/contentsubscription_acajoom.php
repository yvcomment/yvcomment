<?php

/**
* Content Subscription - Acajoom plugin
* @version 1.0.0
* @package ContentSubscription_Acajoom
* @(c) 2009 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license GPL
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

if (strtoupper( $_SERVER['REQUEST_METHOD'] ) == 'HEAD') {
	// hide, cause something works wrong in this case	
} else {
	jimport('joomla.event.plugin');
}

class plgSystemcontentsubscription_acajoom extends JPlugin {
	var $_debug = true;
	var $_authors_subscribe_comments_listid = 0;

	function plgSystemcontentsubscription_acajoom(& $subject, $config) {
		parent :: __construct($subject, $config);
		$this->_debug = (boolean) $this->_getConfigValue('debug', true);
		$this->_authors_subscribe_comments_listid = (int) $this->_getConfigValue('authors_subscribe_comments_listid', '0');
	}

	// Is the $RecipientID User subscribed to comments on his Articles (or Comments)?
	// $isSubscribed - Three-state variable:
	// 2 - means 'default'; false - 'No'; true - 'Yes'
	function onNotifyAuthorOfAddEditComment($RecipientID, & $isSubscribed) {
		$Ok = true;
		$message = '';

		if ($this->_authors_subscribe_comments_listid != 0 ) {
			$database =& JFactory::getDBO();
			$query = 'SELECT q.subscriber_id FROM #__acajoom_queue AS q'
			. ' INNER JOIN #__acajoom_subscribers AS s ON q.subscriber_id = s.id'
			. ' WHERE ( q.list_id = ' . $this->_authors_subscribe_comments_listid 
			. ' ) AND (s.user_id = ' . (int) $RecipientID . ')';
	     	$database->setQuery($query);
	       	$rows = $database->loadObjectList();
	       	$isSubscribed = (count($rows) > 0);
		}

		if ($this->_debug) {
			$message .= 'Content Subscription - Acajoom: ' 
			. 'Event="onNotifyAuthorOfAddEditComment"'
			. '; ListID="' . $this->_authors_subscribe_comments_listid	. '"' 
			. '; RecipientID="' . $RecipientID . '"' 
			. '; isSubscribed="' . ($isSubscribed ? 'Yes' : 'No') . '"';
			
		}

		if (strlen($message) > 0) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($message, ('notice'));
		}
		return $Ok;
	}

	// returns value of the Extension parameter
	function _getConfigValue($paramName = '', $default = '') {
		$value = $default;

		switch ($paramName) {
			case 'access' :
				if ($this->params) {
					// '1' means some lowest level...
					$value = 1;
				} else {
					// if Plugin is not loaded, then Access is denied
					$value = 0; //999; 
				}
				break;
			default :
				if ($this->params) {
					$value = $this->params->get($paramName, $default);
				}
		}
		//echo '_getConfigValue param="' . $paramName . '", value="' . $value . '"<br/>';

		return $value;
	}
}
?>