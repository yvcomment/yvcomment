<?php
/**
 * yvComment - A User Comments Component, developed for Joomla 1.5
 * @version		$Id: install.yvcomment.php 36 2013-02-09 15:44:45Z yvolk $
 * @package yvComment
 * @(c) 2007-2008 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 **/

defined('_JEXEC') or die('Restricted access'); // no direct access

function com_install() {
	$mainframe = JFactory::getApplication();
	$Ok = false;
	$message = "";

	// $message .= 'Initialize yvComment solution <br />';
	if (!class_exists('yvCommentHelper')) {
		$path = JPATH_SITE . DS . 'components' . DS . 'com_yvcomment' . DS . 'helpers.php';
		if (file_exists($path)) {
			require_once ($path);
		}
	}
	if (class_exists('yvCommentHelper')) {
		$Ok = yvCommentHelper::VersionChecks('', '', true);
	}

	if (!$Ok) {
		$message .=
			'<strong>Error!</strong> yvComment component was not installed properly.' 
			. ' Please see the <a href="http://yurivolkov.com/Joomla/yvComment/index_en.html"'
			. ' target="_blank">yvComment\'s Homepage</a>. <br />';
	}
	if (strlen($message) > 0) {
		$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		$message = "";
	}
	if (!$Ok) {
		return false;
	}

	$db = JFactory :: getDBO();
	//$TableName = $db->replacePrefix('#__yvcomment');
	// workaround Joomla! bug
	$TableName = yvCommentJDatabase::replacePrefixStatic($db, '#__yvcomment');
	if (yvCommentHelper::TableExists($TableName)) {
		//($error->get('level') == E_NOTICE) ? 'notice' : 'error';
		$message .=
		str_replace( '%1', $TableName, JText::_( 'TABLE_YVCOMMENT_ALREADY_EXISTS')) . '<br />';
	} else {
		$query = 'CREATE TABLE #__yvcomment (LIKE #__content)';
		$db->setQuery($query);
		$Ok = $db->query();
		$message .= yvCommentHelper::printDbErrors($db);
	}

	// This is a hack in order to see views of this extension in the "New menu item type" window
	// This hack is not needed if extension has backend menu item.
	//$query = "UPDATE `#__components` SET link='" . "option=com_yvcomment" . "' WHERE `option` = 'com_yvcomment'";
	//$db->setQuery($query);
	//$Ok = $db->query();

	if (strlen($message) > 0) {
		$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		$message = "";
	}

	if (!$Ok) {
		//JError :: raiseError(500, $db->stderr());
		return false;
	}
	?>
<fieldset class="adminform"><legend><?php echo JText::_( 'DESCRIPTION'); ?></legend>
<table class="admintable" width="100%">
	<tr>
		<td>

		<p>yvComment is Commenting solution for Joomla! 1.6.0+ only! (Detected
		Joomla! version="<?php echo yvCommentHelper::JoomlaShortVersion(); ?>")</p>
		<p>This package is "yvComment Component" part of yvComment solution.</p>
		<p>It is required to install both "yvComment Component" and "yvComment
		Plugin".</p>
		<p>For the latest and complete information about this extension see <a
			href="http://yurivolkov.com/Joomla/yvComment/index_en.html"
			target="_blank">yvComment's Homepage</a>.</p>
		</td>
	</tr>
</table>
</fieldset>
	<?php
	echo yvCommentHelper::Credits();
	return true;
}
?>