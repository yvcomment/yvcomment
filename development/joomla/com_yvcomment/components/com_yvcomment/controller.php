<?php
/**
 * yvComment - A User Comments Component, developed for Joomla 1.5
 * @version 1.0.0
 * @package yvComment
 * @(c) 2007 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 **/

jimport('joomla.application.component.controller');

class yvcommentController extends JController {
	protected $CommentTypeId = 0;
	// Array (for each CommentTypeId) of arrays (Views)
	//protected static $viewsForCommentTypes = array();
	// Array of arrays (Views) for this CommentTypeId
	//protected $views;
	
	// Button, pressed on the form
	var $_button = '';
	var $_yvCommentID = 0;
	var $_created_by_alias = '';
	var $_created_by_link = '';
	var $_created_by_email = '';
	var $_title = '';
	var $_text = '';
	var $_state = 0;

	var $_model = null;
	var $_view = null;


	var $_AccessEnabled = false;

	function __construct( $config = array() )
	{
		$this->CommentTypeId = intval($config['comment_type_id']);

		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$message = '';
		$Ok = true;
		// Content state filter  1 == Published
		$filter_state		= '1';

		//Joomla doesn't check Access level of component, so let's do it ourselves.
		$user = JFactory::getUser();
		$groups = $user->getAuthorisedViewLevels();
		// THIS is not real Joomla! access level, so level 1 is Ok!
		$access = $yvComment->getConfigValue('access', 0);
		if ($access > 0) {
			$this->_AccessEnabled = true;
		} else {
			echo JText::_('ACCESS_DENIED') . ' User\'s access levels=' . implode(',', $groups) . '<br />';
			return;
		}
		$params =& $yvComment->PageParameters();

		$viewName	= '';
		if ( isset( $config['view'] ) ) {
			$viewName = $config['view'];
			unset( $config['view'] );
		} else {
			$viewName	= JRequest::getVar( 'view', 'unknown');
		}
		//echo 'viewName=' . $viewName . '<br/>';
		$Ok = ($viewName != 'none');

		if ($Ok) {
			$task = '';
			if ( isset( $config['task'] ) ) {
				$task = $config['task'];
				unset( $config['task'] );
			} else {
				$task	= JRequest::getVar( 'task', '');
			}
			if (empty($task)) {
				$task = 'viewdisplay';
			}

			// ToDo: Is this optimal?
			$document = JFactory::getDocument();
			$viewType	= $document->getType();

			// Correct viewName
			switch ($viewName) {
				case 'comment':
				case 'listofcomments' :
				case 'mostcommented' :
					break;
				default:
					//Set default view name
					switch ($yvComment->DisplayTo()) {
						case 'module' :
							$viewName = 'listofcomments';
							break;
						case 'plugin' :
							$viewName = 'comment';
							break;
						default:
							$viewName = 'listofcomments';
					}
			}
			$yvComment->setViewName($viewName);

			parent::__construct($config);

			$Ok = $yvComment->FindContext();
			//echo 'setArticle: ' . print_r($yvComment->_CategoryID, true) . '; User="' . print_r($user, true) . '"' . '; Ok="' . $Ok . '"<br />';
		}

		if ($Ok && $viewName == 'comment') {
			if ($yvComment->IsIdShown($yvComment->getArticleID(), false)) {
				// Info about this article was shown already, so hide this second duplicate.
				if ($yvComment->isDebug()) {
					echo '<hr />Info about this article was shown already, so hide this second duplicate; ArticleID=' . $yvComment->getArticleID() . '<hr />';
				}
				$Ok = false;
			}
		}

		if ($Ok) {
			$context = 'yvComment.' . $viewName ;

			// Content state filter
			if ($yvComment->EditStateAllowed(0)){
				// Code from "/administrator/components/com_content/models/articles.php"
				$f = $mainframe->getUserStateFromRequest( $context.'.filter.state', 'filter_state', '');
				if ($f != '') {
					$filter_state = $f;
				}
			}
			//echo 'filter_state="' . $filter_state . '"<br />';
		}

		if ($Ok && $task != 'viewdisplay') {
			$this->_button = JRequest::getVar('button', '', '', 'STRING');

			$this->_created_by_alias = trim( JRequest::getVar( 'created_by_alias', '', '', 'STRING'));
			$this->_created_by_link = JRequest::getVar( 'created_by_link', '', '', 'STRING');
			$this->_created_by_email = JRequest::getVar( 'created_by_email', '', '', 'STRING');
			$this->_title = JRequest::getVar( 'title', '', '', 'STRING');

			//JREQUEST_ALLOWRAW , JREQUEST_ALLOWHTML
			switch ($yvComment->getConfigValue('allow_html_edit_text', 'no'))
			{
				case 'allowhtml' :
					// This doesn't work with unclosed tags:
					$mask = JREQUEST_ALLOWHTML;
					break;
				case 'allowraw' :
					$mask = JREQUEST_ALLOWRAW;
					break;
				default :
					$mask = 0;
			}
			$this->_text	= JRequest::getVar('text', '', '', 'STRING', $mask);
			//echo 'mask:'. $mask . ';text: "' . $this->_text . '"<br/>';
			//$this->_text	= 'changed <hr>';
			$this->_state	= intval(JRequest::getInt('state', $this->_state));
			 
			$this->_yvCommentID = intval(JRequest::getInt( 'yvCommentID', 0));
			//echo 'yvComment_p001; task="' . $task . '"; ArticleID="' . $yvComment->getArticleID() . '"<br />';
			//echo 'yvCommentID="' . $this->_yvCommentID . '"<br />';
		}

		if ($Ok) {
			if ($yvComment->getConfigValue('execute_content_plugins', '0')) {
				if (!yvCommentHelper::$ContentPluginsImported) {
					yvCommentHelper::$ContentPluginsImported = true;
					JPluginHelper::importPlugin('content');
					//$mainframe->enqueueMessage('Importing content plugins',	'notice');
				}
			}
		}

		if ($Ok) {
			$model = & $this->getModel( $viewName, '', $config );
			if ($model)	{
				$model->setParms($this->_yvCommentID, $task, $filter_state);
			}
			$this->_view = & $this->getView($viewName, $viewType, '', $config);

			// This is how layout is set in Joomla! 2.5
			// TODO: Delete similar lines from other places of the extension
			$layoutName = $yvComment->getPageValue('layout_name', 'default');
			if ($layoutName == '0') {
				$layoutName = $yvComment->getPageValue('layout_name_custom', 'default');
			}			
			$this->_view->setLayout($layoutName);
			// echo ' yvcommentController: Layout=' . $this->_view->getLayout() . '; ';
		}
		if (!$Ok) {
			$this->_AccessEnabled = false;
		}

		if (strlen($message) > 0) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage('yvcommentController: <br />' . $message . '<br />', ( $Ok ? 'notice' : 'error'));
		}

		return;
	}

	function execute( $task ){
		$return = null;
		if ($this->_AccessEnabled) {
			$return = parent::execute( $task );
		}
		return $return;
	}

	// Display raw CAPTCHA
	function displaycaptcha() {
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$captchaType = $yvComment->getConfigValue('use_captcha', 0);
		if ( $captchaType > 0) {
			$Ok = false;
			$document = &JFactory::getDocument();
			switch ($document->getType()) {
				case 'raw':
					switch ($captchaType) {
						case '1':
							$mainframe->triggerEvent('onCaptcha_Display', array($Ok));
							if (!$Ok) {
								// I tried to switch Joomla to raw - no luck: only format=raw in URL works
								//$doc = &JDocument::getInstance('raw');
								//$doc = New JDocument;
								//$doc->setType('raw');
								// Swap the objects
								//$document = $doc;
								// 	header("Content-Type: image/png");

								// 2011-03-09 Joomla! 1.6 has some bug here, see
								// http://forum.joomla.org/viewtopic.php?f=578&t=575696&p=2437312#p2437312
								// so this doesn't work
								// $document->setType('raw');
								// $document->setMimeEncoding('text/plain');

								//We can not redirect image URL...
								//$this->setRedirect($yvComment->getSiteURL() . 'components/com_yvcomment/assets/error_displaying_captcha.png');
								//$this->Redirect();

								$imageFile = JPATH_SITE_YVCOMMENT . DS . 'assets' . DS . 'error_displaying_captcha.png';
								$document->setMimeEncoding('image/png');
								include $imageFile;
								break;
							}
					}
			}
		}
	}

	/**
	 * Inserts Comment to the database
	 */
	function add()
	{
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$Ok = false;
		$IsPublished = false;
		do {
			//echo 'preview=' . JRequest::getVar('preview') . '<br />';
			if ($this->_button == 'preview') {
				$this->_model->setTask('addpreview');
				// Preview
				$this->_model->AddOrPreview($this->_title, $this->_text,
				$this->_created_by_alias, $this->_created_by_link,
				$this->_created_by_email,
				true, $IsPublished);
				break;
			} elseif ($this->_button == 'post') {
				if (!$this->_model->AddOrPreview($this->_title, $this->_text,
				$this->_created_by_alias, $this->_created_by_link,
				$this->_created_by_email,
				false, $IsPublished)) {
					$this->_model->setTask('addpreview');
					break;
				}
				if (!$IsPublished) {
					// Don't redirect: show message before continue...
					break;
				}
			}
			if  ($yvComment->getReturnURL()) {
				//Close
				$this->setRedirect(base64_decode($yvComment->getReturnURL()));
				$this->Redirect();
				return true;
			}

			// Message is formed by the Model!
			//setMessage(JText::_('COMMENT ADDED'));
			$Ok = true;
		} while(0);

		$this->execute('display');
		return $Ok;
	}

	/**
	 * Edit Comment in the database
	 */
	function edit()
	{
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$Ok = false;
		do {
			//echo 'preview=' . JRequest::getVar('preview') . '<br />';
			if ($this->_button == 'preview') {
				$this->_model->setTask('editpreview');
				// Preview
				$this->_model->EditOrPreview($this->_title, $this->_text,
				$this->_created_by_alias, $this->_created_by_link,
				$this->_created_by_email,
				$this->_state, true);
				break;
			} elseif ($this->_button == 'post') {
				if (!$this->_model->EditOrPreview($this->_title, $this->_text,
				$this->_created_by_alias, $this->_created_by_link,
				$this->_created_by_email,
				$this->_state, false)) {
					$this->_model->setTask('editdisplay');
					break;
				}
			}
			if  ($yvComment->getReturnURL()) {
				//Close
				$this->setRedirect(base64_decode($yvComment->getReturnURL()));
				$this->Redirect();
				return true;
			}

			// Message is formed by Model!
			//setMessage(JText::_('COMMENT_MODIFIED'));
			//$this->setRedirect($url, JText::_('COMMENT_MODIFIED'));
			$Ok = true;
		} while(0);

		$this->execute('display');
		return $Ok;
	}

	/**
	 * Delete/Trash Comment
	 */
	function delete()
	{
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$Ok = false;

		do {
			$user	= JFactory::getUser();
			if ($user->get('guest')) {
				JError::raiseError( 403, JText::_( 'ALERTNOTAUTH'));
				break;
			}

			if ($this->_button == 'delete') {
				if (!$this->_model->Delete()) {
					$this->_model->setTask('deletedisplay');
					break;
				}
			}
			if  ($yvComment->getReturnURL()) {
				//Close
				$this->setRedirect(base64_decode($yvComment->getReturnURL()));
				$this->Redirect();
				return true;
			}

			// Message is formed by Model!
			//setMessage(JText::_('COMMENT_MODIFIED'));
			//$this->setRedirect($url, JText::_('COMMENT_MODIFIED'));
			$Ok = true;
		} while(0);

		$this->execute('display');
		return $Ok;
	}

	/**
	 * Publish/Unpublish Comment
	 */
	function publish()
	{
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$Ok = false;

		do {
			$user		= JFactory::getUser();
			if ($user->get('guest')) {
				JError::raiseError( 403, JText::_( 'ALERTNOTAUTH'));
				break;
			}

			if (!$this->_model->Publish($this->_state)) {
				$this->_model->setTask('editdisplay');
				break;
			}

			if  ($yvComment->getReturnURL()) {
				//Close
				$this->setRedirect(base64_decode($yvComment->getReturnURL()));
				$this->Redirect();
				return true;
			}

			$Ok = true;
		} while(0);

		$this->execute('display');
		return $Ok;
	}

	/**
	 * Redirect to the plugin mode (return to the Article)
	 */
	function viewdisplay()
	{
		$mainframe = JFactory::getApplication();
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
		$Ok = false;
		if ($yvComment->DisplayTo() == 'component') {
			if  ($yvComment->getReturnURL()) {
				//Close
				$this->setRedirect(base64_decode($yvComment->getReturnURL()));
				$this->Redirect();
				return true;
			}
		}
		$this->execute('display');
		return $Ok;
	}


	function &getModel( $name = '', $prefix = '', $config = array() )
	{
		if (!$this->_model)	{
			$config['comment_type_id'] = $this->CommentTypeId;
			$this->_model = parent::getModel( $name, $prefix, $config );
		}
		return $this->_model;
	}

	// Override to 
	// replace one 'static' array of views
	// with different arrays for different types of comments
	function &getView( $name = '', $type = '', $prefix = '', $config = array() ) {
		static $viewsForCommentTypes;
		if ( !isset( $viewsForCommentTypes )) {
			$viewsForCommentTypes = array();
			// echo 'Array of arrays of views was initialized<br />';
		}
		if ( !isset( $viewsForCommentTypes[$this->CommentTypeId] ) ) {
			$viewsForCommentTypes[$this->CommentTypeId] = array();
			// echo 'Array of views for type=' . $this->CommentTypeId . ' created<br />';
		}
		$views = &$viewsForCommentTypes[$this->CommentTypeId];
		
		if ( empty( $name ) ) {
			$name = $this->getName();
		}

		if ( empty( $prefix ) ) {
			$prefix = $this->getName() . 'View';
		}

		if ( empty( $views[$name] ) )
		{
			if ( $view = $this->createView( $name, $prefix, $type, $config ) ) {
				$views[$name] = & $view;
			} else {
				$result = JError::raiseError(
					500, JText::_( 'View not found [name, type, prefix]:' )
						. ' ' . $name . ',' . $type . ',' . $prefix
				);
				return $result;
			}
		}

		return $views[$name];
	}
	
	function getOutput()
	{
		if ($this->_AccessEnabled) {
			if ($this->_view) {
				return $this->_view->getOutput();
			}
			return JText::_('View object is not set');
		}
	}
}
?>
