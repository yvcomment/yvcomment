<fieldset class="adminform"><legend><?php echo JText::_( 'CREDITS'); ?></legend>
<table class="admintable" width="100%">
	<tr>
		<th width="40%"><?php echo JText::_( 'CREDITS_NAMES'); ?></th>
		<th width="60%"><?php echo JText::_( 'CONTRIBUTION'); ?></th>
	</tr>
	<tr>
		<td class="name"><a href="http://yurivolkov.com/index_en.html"
			target="_blank">Yuri Volkov</a></td>
		<td><?php echo JText::_( 'FOUNDER'); ?>, <?php echo JText::_( 'PROJECT_LEADER' ); ?>
		&amp; <?php echo JText::_( 'DEVELOPER'); ?></td>
	</tr>

	<tr>
		<td class="name"><a href="http://fotografika.hu" target="_blank">István Somlai</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Hungarian), v.2.01'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="mailto:dan15i@yahoo.com" target="_blank">Daniel Ignat</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Romanian), v.2.00'; ?></td>
	</tr>
	<tr>
		<td class="name">Florent de l'Hamaide</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (French), v.2.00'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.fittowork.net" target="_blank">Latif</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Arabic), v.2.00'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.sylvis-blog.de" target="_blank">Sylvia Neumann</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (German), v.2.00'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.jokrsolutions.se" target="_blank">JOKR Solutions</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Swedish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.cdario.it" target="_blank">Dario Corro'</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Italian), v.2.00'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://chaos.es" target="_blank">MiOiM</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Spanish), v.2.00'; ?></td>
	</tr>

	<!-- v.2.00.002 2011-03-30 -->
	<tr>
		<td class="name"><a href="marekal@post.pl" target="_blank">Marek 
		Pietraszek</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Polish), v.1.25.003'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://joomweb.de" target="_blank">JOOM!WEB - Webservice Olaf Dryja</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (German), v.1.25.003'; ?></td>
	</tr>

	<!-- v.1.25.000 2010-05-19 -->
	<tr>
		<td class="name"><a href="mailto:aitorpena@gmail.com" target="_blank">Aitor
		Peña</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Basque), v.1.24'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="mailto:nturcan@gmail.com" target="_blank">Nicolae
		Turcan</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Romanian), v.1.24'; ?></td>
	</tr>

	<!-- v.1.24.000 2009-08-30 -->
	<tr>
		<td class="name"><a href="http://frustless.de" target="_blank">lemur1
		(Leonid Ratner)</a></td>
		<td><?php echo JText::_( 'DEVELOPER') . ' - idea of Acajoom integration'; ?>
		</td>
	</tr>
	<tr>
		<td class="name"><a href="mailto:mnemonic@get2net.dk" target="_blank">Mnemonic
		(Brian F. Knutsson)</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Danish), v.1.23'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.woraif.cz" target="_blank">Milan
		Šedý</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Czech)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.kastenmaier.de" target="_blank">Ragnar
		Kastenmaier</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (German), v.1.23.0'; ?></td>
	</tr>

	<!-- v.1.22.000 2009-05-14 -->
	<tr>
		<td class="name"><a href="http://www.joomlacommunity.eu/"
			target="_blank">Translation team of JoomlaCommunity.eu</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Dutch)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.szathmari.hu" target="_blank">Andor
		Szathmári</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Hungarian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.marlboroteam.eu/" target="_blank">Simone
		Cremonesi</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Italian)'; ?></td>
	</tr>

	<!-- v.1.21.000 2009-03-09 -->
	<tr>
		<td class="name"><a href="http://ziolczynski.pl" target="_blank">Tomasz
		Ziółczyński</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Polish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://befria.se" target="_blank">Markus
		Larsson</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Swedish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.igoia.info" target="_blank">Fabio
		Gameleira</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Portuguese/Brasil)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.gencat.cat/ics" target="_blank">Xavier
		Montaña Carreras</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Catalan)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="mailto:asut@takas.lt" target="_blank">Andrius
		Sutkevičius</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Lithuanian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="" target="_blank">FoxZilla</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Finnish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.dzconstantine.com"
			target="_blank">Saber Bousba</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' ( Arabic (Algeria))'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.droit-medical.com"
			target="_blank">Bertrand Hue</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (French)'; ?></td>
	</tr>

	<!-- v.1.20.000 2008-01-25 -->
	<tr>
		<td class="name"><a href="http://webbizltd.com/" target="_blank">founder[at}webbizltd.com</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (English)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.VnEtips.com" target="_blank">VnEtips</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Vietnamese)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://mslonik.pl" target="_blank">mslonik</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Polish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.orion.com.mk" target="_blank">ScHRiLL</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Macedonian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.loopgroepbedum.nl/"
			target="_blank">Peter v.d. Hulst</a></td>
		<td><?php echo JText::_( 'DEVELOPER'); ?></td>
	</tr>

	<!-- before 2008-11-30 -->
	<tr>
		<td class="name"><a href="http://www.rijnieks.lv" target="_blank">Krišjānis
		Rijnieks</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Latvian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://webdesignzone.net" target="_blank">Zoran
		Ilić</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Serbian-latin)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="" target="_blank">Pawel Frankowski</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Polish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="" target="_blank">Arno Becht</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Dutch)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://tomi.malensek.com" target="_blank">Tomi
		Malenšek</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Slovenian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://klug.gr" target="_blank">Ziouzios
		Dimitrios and Stefanidis Fotios</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Greek)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://uskk.info" target="_blank">Vitalij
		Mojsejiv</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Ukrainian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.triangulodigital.com"
			target="_blank">Rui Braz</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (European Portguese)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.joomlagate.com/" target="_blank">baijianpeng</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Chinese)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.layan.us" target="_blank">Massalha
		Shady</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Arabic)'; ?></td>
	</tr>

	<!-- before v.19.0 -->
	<tr>
		<td class="name"><a href="http://www.pleijerit.net" target="_blank">Axel
		Toivonen</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Finnish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="" target="_blank">louBaN</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Galician)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://joomla.co.il" target="_blank">Yair
		Lahav</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Hebrew)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://primavolta.hr" target="_blank">Tomislav
		Konestabo</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Croatian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.ibrary.co.kr" target="_blank">Opiokane</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Korean)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="yvcomment@chris.misker.nl" target="_blank">Chris
		Misker</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Dutch)'; ?></td>
	</tr>

	<tr>
		<td class="name"><a href="" target="_blank">vbr_82</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Bulgarian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.arneespedal.com" target="_blank">Arne
		Buchholdt Espedal</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Norwegian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.zemj.com" target="_blank">ZemJ</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Czech)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.ircaserta.com" target="_blank">Sergio
		De Falco aka SGr33n</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Italian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.szathmari.hu" target="_blank">Andor
		Szathmári</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Hungarian)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://florent.nouvellon.net"
			target="_blank">Florent NOUVELLON</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (French)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://pcw.se" target="_blank">Jerry Norén</a>
		</td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Swedish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.revealmusic.se/" target="_blank">Mikael
		Karlsson (aka Mika3l)</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Swedish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.beza.com.ar" target="_blank">Enrique
		F. Becerra</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Spanish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.wsonline.nl" target="_blank">Wim
		Strik</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Dutch)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.peterschluter.dk" target="_blank">Peter
		Schlüter</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Danish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://sefiroth.de/HomePage/Norman"
			target="_blank">Norman Markgraf</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (German)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="mailto:sheldon.young@gmail.com"
			target="_blank">Sheldon Young</a></td>
		<td><?php echo JText::_( 'DEVELOPER'); ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://www.cmsturk.net" target="_blank">Asthon
		and Ercan Özkaya (CmsTürk)</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Turkish)'; ?></td>
	</tr>
	<tr>
		<td class="name"><a href="http://joomfa.org" target="_blank">Joomfa
		Team</a></td>
		<td><?php echo JText::_( 'TRANSLATOR') . ' (Persian/Farsi)'; ?></td>
	</tr>
</table>
</fieldset>
