<?php
/**
 * This is dummy class for raw output (in 'displaycaptcha' task...)
 * @version		$Id: view.raw.php 19 2010-05-25 15:05:48Z yvolk $
 * @package yvComment
 * @(c) 2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 **/
defined('_JEXEC') or die( 'Restricted access' );

if (function_exists('jimport')) {
	// yvolk 2008-07-09 Somehow it is not found sometimes...
	jimport( 'joomla.application.component.view');
}

class yvcommentViewListofcomments extends yvCommentJView
{
	public $CommentTypeId = 0;
	var $_doEcho = true;
	// '', 'plugin', 'module'
	var $_DisplayTo = '';

	function __construct($config = array())	{
		parent::__construct($config);
		$this->CommentTypeId = intval($config['comment_type_id']);
	}

	function getOutput()
	{
		return $this->_output;
	}

	function display( $tpl = null)
	{
			
	}
}
?>