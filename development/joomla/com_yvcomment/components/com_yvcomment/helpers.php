<?php
/*
 * yvComment - Commenting solution
 * @version		$Id: helpers.php 40 2013-07-14 13:38:02Z yvolk $
 * @package		yvCommentComponent
 * @copyright	2007-2011 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
 * @license GPL
 */
define('JPATH_SITE_YVCOMMENT', dirname(__FILE__) );

require_once (JPATH_SITE_YVCOMMENT . DS . 'controller.php');

// Required for function ContentIDToURL
require_once (JPATH_SITE.DS.'includes'.DS.'application.php' );
require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

// For yvCommentJView
jimport( 'joomla.application.component.view');

// Contains global yvComment parameters etc.
class  yvCommentHelper extends JObject {
	// Each instance of this class is created for each Comment Type
	// use getInstance(CommentTypeId)
	private static $_CommentTypeInstances = array();
	/**
	 * Stylesheets added to the document
	 * This variable helps avoid adding the same stylesheet twice
	 */
	private static $_Stylesheets = array();

	// Main Release Level. Extensions for the same Release are compatible
	private static $_Release = '2.01';
	// Sub Release Level (Service release)
	private static $_Dev_level = '1';

	public static $ContentPluginsImported = false;

	private static $_ShowLogo = false;
	// Is yvComment logo already put to the HTML page
	private static $_IsLogoShown = false;

	/**
	 *  True e.g. for User who was authenticated with OpenId,
	 *  but who wasn't added to Joomla! user's automatically after login
	 */
	private static	$_UserIsTemp = false;
	/**
	 *  Is Current User a Guest? (i.e. not registered)
	 */
	private static	$_UserIsGuest = true;

	//------------------------------------------------------------
	/** Common attributes - the same for all instances of yvComment,
	*  i.e. for instances of vComment modules, component and plugins
	*  on the same HTML page.
	*/
	// This variable distinguishes different yvComment instances in one Joomla! site
	// Should be > 0
	private $CommentTypeId;

	// Main debug switch. Set it to true to debug the whole yvComment solution
	private $_debug = false;
	// Debug security related issues
	private $_debugSec = false;

	private $_Ok = true; // if false - error state: try to be quiet
	// id='yvComment' . ArticleID already put to the HTML page
	private $_IdShown = array();

	var $_pluginData = null;
	var $_pluginParams = null;
	var $_UseContentTable = false;
	var $_UseDesignatedCategoryForComments = false;
	var $_CategoryForComments = 0;
	// Table name for comments
	var $_TableName = '#__content';

	/**
	 * ID of the Guest User (==0 if guests are not allowed to add comments)
	 */
	private $_GuestID = 0;
	
	/**
	 * first index - Comment ID, [0] - for all comments
	 * second index - action (short 'create', 'edit' ...)
	 * value - encoded boolean ('Allowed'): 1 - true; 2 - false
	 */
	private $allowedActions = array();

	// Used by AddAllowedForArticle function
	private $AddAllowedForArticle_ArticleID = null;
	private $AddAllowededForArticle_Enabled = null;
	
	var $_IsPaginationEnabled = false;
	// Stored limit value (for debug purposes...)
	var $_limit1 = 0;

	// Where to return after editing of comment?
	var $_ReturnURL = '';
	// At which Web page (Menu Itemid) we should show component?
	var $_ComponentItemid = 0;

	// Some vars from component
	var $_Comp_option = '';
	var $_Comp_view =  '';
	var $_Comp_id	= 0;

	// Total number of instances of this Type Of Comment,
	//   initialized during this (Web page) request
	var $_nInd = 0;
	// Index of attributes, that belongs to current instance
	// -1 - there is no instances yet
	// 0 - first initialized instance
	var $_Ind = -1;

	//------------------------------------------------------------
	// Instance-specific attributes
	var $_IndPrev = array();  // integer, Index of previous instance

	// Option, that called yvComment
	var $_ParentOption = array();  // string
	var $_ParentView = array();  // string

	var $_InstanceId = array();  // integer
	var $_DisplayTo = array(); // string
	var $_PageParameters = array();
	var $_ViewName = array();

	var $_FilterByContext = array();
	var $_article = array();
	var $_CategoryID = array();
	var $_ArticleID = array();

	/*
	 * Initialize common attributes - the same for all instances of yvComment
	 */
	function __construct($CommentTypeId) {
		$mainframe = JFactory::getApplication();
		$this->CommentTypeId = $CommentTypeId;
		$message = '';
		$db = JFactory::getDBO();
		$this->_limit1 = intval(JRequest::getInt('limit', '-1')) ;

		// Get Plugin info
		// Every Comment Content Type has it's own copy of plugin parameters!
		$this->_pluginData = JPluginHelper::getPlugin('content', 'yvcomment' . (($this->CommentTypeId == 1) ? "" : $this->CommentTypeId));
		if ($this->_pluginData) {
			$this->_pluginParams = new JRegistry( $this->_pluginData->params );
			$this->_debug = (bool) $this->getConfigValue('debug', '0');
			$this->_debugSec = (bool) $this->getConfigValue('debug_sec', '0');
			//echo 'pluginData: ' . print_r($this->_pluginData, true);
		} else {
			$this->_pluginParams = new JRegistry('');
			if (!$mainframe->isAdmin()) {
				$this->_Ok = false;
			}
		}

		$lang = JFactory::getLanguage();
		// 2007-03-23 v.1.15 - language file moved to the administrator site
		//$lang->load('com_yvcomment', JPATH_SITE);
		$langResult1 = false;
		if ($this->isDebug()) {
			// Try to load THIS language file
			$langResult1 = $lang->load('com_yvcomment', JPATH_ADMINISTRATOR, $lang->getTag(), true ,false);
		}
		$langResult2 = $langResult1;
		if (!$langResult2) {
			$langResult2 = $lang->load('com_yvcomment', JPATH_ADMINISTRATOR);
		}

		if ($this->isDebug() || !$this->_pluginData ) {
			$message .= 'yvComment plugin type=' . $this->CommentTypeId . ' is ' . ($this->_pluginData ? 'loaded' : 'NOT loaded') . '<br/>';
		}
		if ($this->isDebug()) {
			$session = JFactory::getSession();
			$this->log('yvComment::__construct: ' .
			print_r($session, true) .
				'$_SESSION: ' .
			print_r($_SESSION, true)
			);
			$message .= 'Language strings for \'' . $lang->getTag() . '\' were ' . ($langResult1 ? '' : ' NOT') . ' loaded.<br/>';
			if (!$langResult2) {
				$message .= 'No Language strings were loaded.<br/>';
			}
		}

		$this->_UseDesignatedCategoryForComments = (bool) $this->getConfigValue('usedesignatedcategory', '1');
		if ($this->UseDesignatedCategoryForComments()) {
			$this->_CategoryForComments = $this->getConfigValue('categoryid', '0');
		}
			
		if ($this->getConfigValue('usecontenttable', '1')) {
			$this->_UseContentTable = true;
			$this->_TableName = $db->getPrefix() . 'content';
		} else {
			$this->_TableName = $db->getPrefix() . 'yvcomment';
		}

		if ($this->getConfigValue('allow_guest_add', 0)) {
			$guest_username = $this->getConfigValue('guest_username', '');
			if (!empty($guest_username)) {
				jimport('joomla.user.helper');
				$id = JUserHelper::getUserId($guest_username);
				$this->_GuestID = $id;
			}
			if ($this->_GuestID == 0) {
				$message .= JText::_('GUEST_USER_ACCOUNT_ERROR') . ' guest_username="' . $guest_username . '"<br />';
			}
		}

		$this->storeOptionParms();
		$this->LookupUser();
			
		$css = $this->getConfigValue('usecss','1');
		switch ($css)
		{
			case '0' :
				// don't use CSS
				break;
			case '1' :
				$css = 'style001.css';
			default :
				$cssUrl = $this->getSiteURL() . 'components/com_yvcomment/assets/' . $css;
				$found = false;
				foreach (self::$_Stylesheets as $styleSheet) {
					if ($styleSheet == $cssUrl) {
						$found = true;
						break;
					}
				}
				if (!$found) {
					// Add the same stylesheet only once
					self::$_Stylesheets[] = $cssUrl;
					$doc = JFactory::getDocument();
					$doc->addStyleSheet( $cssUrl, 'text/css');
				}

				// Do not add the script until it has some useful functionality to
				// reduce the number of HTTP requests.
				// $doc->addScript( $url . 'components/com_yvcomment/assets/default.js', 'text/javascript');
		}
			
		if (strlen($message) > 0) {
			$message .= self::_textSignature();
			$mainframe->enqueueMessage($message, ($this->_Ok ? 'notice' : 'error'));
		}
	}

	/**
	 * Returns a reference to the global yvCommentHelper object,
	 * that represents Comments of Type == $CommentTypeId
	 * only creating it if it doesn't already exist.
	 * CommentTypeId should be 1, 2, ...
	 * missed optional CommentTypeId value is always treated as == 1
	 */
	public static function &getInstance($CommentTypeId = 1)
	{
		$instance = null;
		$CommentTypeId = intval( $CommentTypeId);
		if ($CommentTypeId == 0) {
			$CommentTypeId = 1;
		}
		if ($CommentTypeId > 0) {
			if ( !isset( self::$_CommentTypeInstances[$CommentTypeId] ) ) {
				self::$_CommentTypeInstances[$CommentTypeId]
				= new yvCommentHelper($CommentTypeId);
			}
			$instance =	self::$_CommentTypeInstances[$CommentTypeId];
		}
		return $instance;
	}

	/**
	 * Find and cache for future use some parameters of current User
	 */
	public function LookupUser() {
		$message = '';
		self :: $_UserIsTemp = false;

		$user	= JFactory::getUser();
		// Based on code from 'plugins/user/joomla.php', function onLoginUser
		if ($user->get('tmp_user') == 1) {
			self :: $_UserIsTemp = true;
		}
		self :: $_UserIsGuest = (bool) $user->guest;
		// echo 'user: ' . print_r($user, true);

		if ($this->_debugSec) {
			if ($this->UserIsTemp()) {
				$message .= 'User is temp: <br />' . print_r($user, true) . '<br />';
			}
		}
		if (strlen($message) > 0) {
			$mainframe = JFactory::getApplication();
			$mainframe->enqueueMessage($message, ('notice'));
		}
	}

	function storeOptionParms() {
		$option = JRequest::getCmd('option');
		if (empty($this->_Comp_option)) {
			if ($option != 'com_yvcomment') {
				$this->_Comp_option = $option;
				$this->_Comp_view =  JRequest::getCmd( 'view' );
				$this->_Comp_id	= intval(JRequest::getInt('id'));
			}
		}
	}

	/*
	 * Initialize Instance-specific attributes
	 * returns: Instance index
	 */
	function BeginInstance($DisplayTo, &$params) {
		$option = JRequest::getCmd('option');
		$mainframe = JFactory::getApplication();

		$this->_nInd += 1;
		$InstanceInd = $this->_nInd - 1;

		// push (store) previous index
		$this->_IndPrev[$InstanceInd] = $this->_Ind;

		$this->_Ind = $InstanceInd;

		//Store current global variables
		$this->_ParentOption[$InstanceInd] = $option;
		$this->_ParentView[$InstanceInd] = JRequest::getCmd('view');
		$this->_ParentLayout[$InstanceInd] = JRequest::getCmd('layout');

		$this->_InstanceId[$InstanceInd] = rand(1000,9999);
		$this->_DisplayTo[$InstanceInd] = $DisplayTo;
		$this->_PageParameters[$InstanceInd] = null;
		$this->_ViewName[$InstanceInd] = 'none';

		$this->_FilterByContext[$InstanceInd] = 'allcategory';
		$this->_article[$InstanceInd] = null;
		$this->_CategoryID[$InstanceInd] = 0;
		$this->_ArticleID[$InstanceInd] = 0;

		$this->_setPage($params);

		$option = 'com_yvcomment';
		JRequest::setVar('option', $option);

		if ($InstanceInd == 0) {
			$this->_ReturnURL	= JRequest::getVar('url', '', '', 'STRING');
			//echo 'url="' . $this->_ReturnURL . '"<br />';
			$this->_ComponentItemid = intval(JRequest::getInt('Itemid', 0));
			if ($this->isDebug()) {
				$message = 'Itemid="'	. $this->_ComponentItemid . '"; ';
				$message = 'url="'	. base64_decode($this->getReturnURL(false)) . '"';
				$mainframe->enqueueMessage($message, 'notice');
			}

			if ($DisplayTo == 'component') {
				$this->_IsPaginationEnabled = true;
			} else {
				$PageParameters = $mainframe->getPageParameters();
				if (!$PageParameters->get('show_pagination')) {
					// Show pagination for Comments only if Component... or something else
					// didn't show pagination
					$this->_IsPaginationEnabled = true;
				}
				if ($this->isDebug()) {
					$message = 'PageParameters="'	. print_r($PageParameters, true) . '"';
					$mainframe->enqueueMessage($message, 'notice');
				}
			}
		}

		if ($DisplayTo == 'component'
		&& $this->_ParentOption[$InstanceInd] == 'com_yvcomment') {
			if ($this->getComponentItemid() != 0) {
				// 2008-11-03, Joomla! 1.5.7
				// It looks like sometimes Joomla doesn't parse Itemid value
				// so we need to do it ourselves:
				$router = JSite::getRouter();
				$itemid = (integer) $router->getVar('Itemid');
				if ($itemid == 0) {
					$app = JApplication::getInstance('site');
					$menu = $app->getMenu(true);
					//echo '<hr>menu=' . print_r($menu, true) . '<hr>';
					if ($this->isDebug()) {
						$menuActive = $menu->getActive();
						$message = 'Joomla! didn\'t parse Itemid (bug?):'
						. ' Parsed Itemid=' . $itemid
						. '; in Request Itemid=' . $this->getComponentItemid()
						. '; ActiveMenuItemID=' . ( $menuActive ? $menuActive->id : "(not set)" );
						$mainframe->enqueueMessage($message, 'notice');
					}
					$menu->setActive($this->getComponentItemid());
				}
			}
		}

		if ($this->isDebug()) {
			$mainframe = JFactory::getApplication();
			$message = 'BeginInstance type=' . $this->CommentTypeId . '; ind='	. $InstanceInd
			. '; IndPrev=' . $this->_IndPrev[$InstanceInd]
			. '; DisplayTo="' . $DisplayTo
			. '"; moduleclass_sfx="' . $this->getPageValue('moduleclass_sfx','(not set)')
			. '"; ParentOption="' . $this->_ParentOption[$InstanceInd]
			. '"; ParentView="' . $this->_ParentView[$InstanceInd]
			. '"; ParentLayout="' . $this->_ParentLayout[$InstanceInd] . '"';
			$mainframe->enqueueMessage($message, 'notice');
		}
		return $this->_Ind;
	}

	// This instance wan't be called, but memory is not freed
	function EndInstance( $InstanceInd ) {
		if ($this->isDebug()) {
			$mainframe = JFactory::getApplication();
			$message = 'EndInstance ind='	. $InstanceInd;
			$mainframe->enqueueMessage($message, ('notice'));
		}
		if ($InstanceInd >= 0) {
			if ($this->_Ind != $InstanceInd) {
				$mainframe = JFactory::getApplication();
				$message = 'Order of Instances is wrong: ind='
				. $InstanceInd . ' is being ended, but current ind="' . $this->_Ind . '"';
				$message .= self::_textSignature();
				$mainframe->enqueueMessage($message, ('notice'));

				// Trying to fix...
				$this->_Ind = $InstanceInd;
			}

			// Restore global values for use by other extensions
			JRequest::setVar('layout', $this->_ParentLayout[$InstanceInd]);
			JRequest::setVar('view', $this->_ParentView[$InstanceInd]);
			$option = $this->_ParentOption[$InstanceInd];
			JRequest::setVar('option', $option);
				
			// pop (restore) previous index
			$this->_Ind = $this->_IndPrev[$this->_Ind];
		}
	}

	function Ok() {
		return $this->_Ok;
	}

	/**
	 * Check if all Types of comments are Ok
	 */
	public static function OkAll() {
		$Ok = true;
		foreach( self::$_CommentTypeInstances as $ins) {
			if ($ins && !$ins->Ok()) {
				$Ok = false;
				break;
			}
		}
		return $Ok;
	}

	/**
	 * Hide all Types of comments
	 */
	private static function HideAll() {
		foreach( self::$_CommentTypeInstances as $ins) {
			if ($ins) {
				$ins->_Ok = false;
			}
		}
	}

	function InstanceId() {
		return $this->_InstanceId[$this->_Ind];
	}

	function ParentView() {
		return $this->_ParentView[$this->_Ind];
	}

	function ParentOption() {
		return $this->_ParentOption[$this->_Ind];
	}
	function IsNested() {
		return (
		$this->_IndPrev[$this->_Ind] >= 0
		) ;
	}

	function DisplayTo() {
		return $this->_DisplayTo[$this->_Ind];
	}

	function UseContentTable() {
		return $this->_UseContentTable;
	}

	function UseDesignatedCategoryForComments() {
		return $this->_UseDesignatedCategoryForComments;
	}

	function getTableName() {
		return $this->_TableName;
	}

	// Build return URL based on current URL
	function buildReturnURL($UseExistingIfNotEmpty = false, $fragment = "") {
		if ($this->_ReturnURL && $UseExistingIfNotEmpty) {
			$url = $this->_ReturnURL;
		} else {
			$uri = JFactory::getURI();
			if (strlen($fragment) > 0) {
				$uri->setFragment($fragment);
			}
			$url = base64_encode($uri->toString());
		}
		return $url;
	}

	function setReturnURL($url) {
		$this->_ReturnURL = $url;
	}

	function getReturnURL($BuildIfEmpty = false, $fragment = "") {
		if (!$this->_ReturnURL && $BuildIfEmpty) {
			$this->_ReturnURL = $this->buildReturnURL(false, $fragment);
		}
		return $this->_ReturnURL;
	}

	function getComponentItemid() {
		return $this->_ComponentItemid;
	}

	/*
	 * Returns value of Component/Plugin parameter
	 * from Common attributes
	 */
	function getConfigValue($paramName = '', $default = '') {
		$value = $default;

		switch ($paramName) {
			case 'access':
				if ($this->_pluginData) {
					// '1' means some lowest level...
					$value = 1;
				} else {
					// if Plugin is not loaded, then Access is denied
					$value = 0; //999;
				}
				break;
			default:
				if ($this->_pluginParams) {
					$value = $this->_pluginParams->get($paramName, $default);
				}
		}
		//echo 'getConfigValue param="' . $paramName . '", value="' . $value . '"<br/>';

		return $value;
	}

	/**
	 * returns Category Id or 0 if category was not assigned to Comments
	 */
	function getCategoryForComments() {
		return $this->_CategoryForComments;
	}

	/**
	 * returns Id of the Type Of Comment
	 */
	function getCommentTypeId() {
		return $this->CommentTypeId;
	}

	function IsIdShown($ArticleID, $ShowNow = false) {
		$OldValue = false;
		foreach ($this->_IdShown as $id1) {
			if ($id1 == $ArticleID) {
				$OldValue = true;
				break;
			}
		}
		if (!$OldValue && $ShowNow) {
			$this->_IdShown[] = $ArticleID;
		}
		return $OldValue;
	}

	// Flag to show Logo on current page
	public static function setShowLogo($ShowLogo = true) {
		$OldValue = self::$_ShowLogo;
		self::$_ShowLogo = $ShowLogo;
		return $OldValue;
	}

	// Do we need to show Logo now?
	function getShowLogo($ShowNow = true) {
		$ShowLogo = false;
		if ((self::$_ShowLogo) && !$this->IsNested()) {
			if (!self::$_IsLogoShown) {
				$ShowLogo = true;
				if ($ShowNow) {
					self::$_IsLogoShown = true;
				}
			}
		}
		return $ShowLogo;
	}

	function IsPaginationEnabled() {
		return $this->_IsPaginationEnabled;
	}

	function isDebug() {
		return $this->_debug;
	}

	/**
	 * Is a Comment the Content Item from Content Table with this ID?
	 * $id - id from the Content table
	 */
	function IsCommentByID($id)
	{
		$Is = false;
		if ($id != 0) {
			if (!$this->UseContentTable()) {
				// In this case anything in Content Table is not comment
				$Is = false;
			} else if ($this->UseDesignatedCategoryForComments()) {
				//Unfortunately, this is not preserved by editors:
				//   $attribs = new JRegistry( $row->attribs );
				//   if ( $attribs->get('contenttype', 'default') == 'yvcomment') {
				//... so we use CategoryID as ContentTypeID
					
				$categoryid = intval($this->DLookup('catid', '#__content', 'id=' . $id));
				if (($categoryid != 0) &&
				($categoryid == $this->_CategoryForComments)) {
					$Is = true;
				}
			} else {
				$parentid = intval($this->DLookup('parentid', '#__content', 'id=' . $id));
				if (($parentid != 0) ) {
					$Is = true;
				}
			}
		}
		//echo 'IsCommentByID=' . $Is . '; categoryid=' . $categoryid . ' (' . $this->_CategoryForComments . ')<br/>';
		return $Is;
	}

	function setArticle(& $article) {
		$this->_article[$this->_Ind] = & $article;
		$this->_CategoryID[$this->_Ind] = 0;
		$this->_ArticleID[$this->_Ind] = 0;
		//echo 'setArticle: ' . print_r($article, true) . '<br />';
			
		if (is_object( $article )) {
			if (isset( $article->id )) {
				$this->_ArticleID[$this->_Ind] = intval($article->id);
			}
			if (isset( $article->catid )) {
				$this->_CategoryID[$this->_Ind] = intval($article->catid);
			}
		}
	}

	function &getArticle() {
		return $this->_article[$this->_Ind];
	}

	function setViewName( $viewName ) {
		$this->_ViewName[$this->_Ind] = $viewName;

		JRequest::setVar('view', $viewName);
		//echo 'after JRequest::setVar(view, $viewName): ' . JRequest::getVar( 'view', '(unknown)') . '<br />';
	}
	function getViewName() {
		return $this->_ViewName[$this->_Ind];
	}

	function setFilterByContext( $FilterByContext ) {
		$this->_FilterByContext[$this->_Ind] = $FilterByContext;
		$this->setPageValue('filterbycontext', $FilterByContext);
	}
	function getFilterByContext() {
		return $this->_FilterByContext[$this->_Ind];
	}

	function getContextObjectName() {
		$name = '';
		switch ($this->getFilterByContext()) {
			case 'article':
				$id = $this->_ArticleID[$this->_Ind];
				$name = $this->DLookup('title','#__content','id=' . $id);
				break;
			case 'category':
				$id = $this->_CategoryID[$this->_Ind];
				$name = $this->DLookup('title','#__categories','id=' . $id);
				break;
			case 'auto':
			case 'autocategory':
			case 'all':
			default:
				$name = 'none (' . $this->getFilterByContext() . ')';
		}
		return $name;
	}

	function setArticleID( $id ) {
		//intval - to avoid data manipulation
		$this->_ArticleID[$this->_Ind] = intval($id);
	}
	function getArticleID() {
		return $this->_ArticleID[$this->_Ind];
	}

	function setCategoryID( $id ) {
		$this->_CategoryID[$this->_Ind] = intval($id);
	}
	function getCategoryID() {
		return $this->_CategoryID[$this->_Ind];
	}

	function IsFilterByContextOk() {
		$Ok = true;
		switch ($this->_FilterByContext[$this->_Ind]) {
			case 'article':
				if ($this->_ArticleID[$this->_Ind] == 0) {
					$Ok = false;
				}
				break;
			case 'category':
				if ($this->_CategoryID[$this->_Ind] == 0) {
					$Ok = false;
				}
				break;
			case 'auto':
			case 'autocategory':
				$Ok = false;
				break;
			case 'all':
				// Ok!
				break;
			default:
				$Ok = false;
		}
		return $Ok;
	}

	/**
	 * Where are we?
	 */
	function FindContext() {
		$FilterByContext0 = $this->getPageValue('filterbycontext','all');
		$FilterByContext = $FilterByContext0;
		switch ($this->getViewName()) {
			case 'comment':
				if ($FilterByContext != 'article') {
					//echo 'FindContext: \'Filter list by\' option was set to \'article\'<br />';
					$FilterByContext = 'article';
				}
			case 'listofcomments' :
				break;
			default:
		}

		$methods = array();
		switch ($FilterByContext) {
			case 'auto':
			case 'autocategory':
				$methods[] = 'FromRequestArticle';
				$methods[] = 'FromRequest';
				//$methods[] = 'FromArticle';
				$methods[] = 'all';  // may need two passes...
				$methods[] = 'all';  // may need two passes...
				$methods[] = 'all';  // last check
				break;
			case 'all':
				$methods[] = 'all';  // last check
				break;
			default:
				$methods[] = 'FromArticle';
				$methods[] = 'FromRequestArticle';
				$methods[] = 'FromRequest';
				$methods[] = 'FromKnownContextVars';
		}

		foreach ($methods as $method) {
			switch ($FilterByContext) {
				case 'auto':
					if ($this->_ArticleID[$this->_Ind] != 0) {
						$FilterByContext = 'article';
					} elseif ($this->_CategoryID[$this->_Ind] != 0) {
						$FilterByContext = 'category';
					}
					break;
				case 'autocategory':
					if ($this->_CategoryID[$this->_Ind] != 0) {
						$FilterByContext = 'category';
					} elseif ($this->_ArticleID[$this->_Ind] != 0) {
						$method = 'FromKnownContextVars';
					}
					break;
			}
			$this->setFilterByContext($FilterByContext);
			if ($this->IsFilterByContextOk()) {
				break; // break 'foreach' loop
			}

			switch ($method) {
				case 'FromArticle':
					$article = $this->getArticle();
					if (!is_object( $article )) {
						// Find first article of the component
						$article = $this->getArticleOfComponent();
						if (is_object( $article )) {
							$this->setArticle($article);
						}
					}
					break;
				case 'FromRequestArticle':
					if ($this->getArticleID()== 0) {
						$id	= intval(JRequest::getInt( 'ArticleID', 0));
						if ($id == 0) {
							// If this page was accessed through MenuItem link...
							$id	= $this->getPageValue('articleid', 0);
						}
						$this->setArticleID($id);
					}
					break;
				case 'FromRequest':
					if ($this->isDebug()) {
						echo 'FindContext-FromRequest: ' . $this->_Comp_view . '; id=' . $this->_Comp_id . '<br />';
					}
					switch ($this->_Comp_view) {
						case 'article':
							$this->setArticleID($this->_Comp_id);
							break;
						case 'category':
							$this->setCategoryID($this->_Comp_id);
							break;
					}
					break;
				case 'FromKnownContextVars':
					if ($this->isDebug()) {
						echo 'FindContext-FromKnownContextVars: ' . $FilterByContext . '; ArticleID=' . $this->getArticleID() . '<br />';
					}
					switch ($FilterByContext) {
						case 'article':
							// Nothing to do?
							$CommentID = intval(JRequest::getInt( 'yvCommentID', 0));
							if ($CommentID != 0) {
								$ArticleID = $this->DLookup('parentid', $this->getTableName(), 'id=' . $CommentID);
								$this->setArticleID($ArticleID);
							}
							break;
						case 'autocategory':
						case 'category':
							if ($this->getArticleID() != 0) {
								$CategoryOfArticleID = $this->DLookup('catid','#__content','id=' . $this->getArticleID());
								$this->setCategoryID($CategoryOfArticleID);
								if ($this->isDebug()) {
									echo 'CategoryID=' . $CategoryOfArticleID . '<br />';
								}
								if ($FilterByContext=='autocategory') {
									if ($CategoryOfArticleID != '0') {
										$FilterByContext='category';
									}
								}
							}
							break;
					}
					break;
				case 'all':
					$FilterByContext = 'all';
					break;
			}
		}
		if ($this->isDebug()) {
			echo 'FindContext: type=' . $this->CommentTypeId . '; filter0=' . $FilterByContext0 . '; context=' . $this->getFilterByContext() . ', ' . ($this->IsFilterByContextOk() ? 'Ok' : 'Failed') . '<br />';
		}
		return $this->IsFilterByContextOk();
	}

	/*
	 * Number of Comments for the Current context
	 * filter_state: "1" - Published
	 * "0" - Unpublished
	 * "2" - Archived
     * "-2" - Trashed
     * "*" - All 
	 */
	function getNComments($ArticleID = 0, $filter_state = '', $authoridsfilter = '')
	{
		$nComments = 0;
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());
			
		$From = $this->getTableName() . ' AS c';
		$From = '(' . $From . ') INNER JOIN #__content AS ar ON c.parentid=ar.id' ;

		$Where = '';
		if ($this->UseDesignatedCategoryForComments()) {
			$Where = '(c.catid=' . $this->getCategoryForComments() . ')';
		} else {
			$Where = '(c.parentid<>0)';
		}

		$Where .= ' AND (c.access IN  ('.$groups.'))';
		$Where .= ' AND (ar.access IN  ('.$groups.'))';

		$nDays = $this->ResultDaysToNDays($this->getPageValue('result_days', 'all'));
		if ($nDays > 0) {
			$Where .= ' AND (c.created > ' .
			$this->SecondsFromNowToSQLDate($this->DaysToSeconds($nDays)) . ')';
		}

		if ($ArticleID == 0) {
			switch ($this->getFilterByContext()) {
				case 'article':
					$Where .= ' AND ar.id=' . $this->getArticleID();
					break;
				case 'category':
					$Where .= ' AND ar.catid=' . $this->getCategoryID();
					break;
				default:
					// For the whole site don't show comments on comments on the first level
					$Where .= ' AND ar.parentid=0';
			}
		} else {
			$Where .= ' AND c.parentid=' . $ArticleID;
		}

		if (!empty($authoridsfilter)) {
			$Where .= ' AND (c.created_by IN(' . $authoridsfilter . '))';
		}

		//Filter state of both Articles AND Comments
		//See similar 'if condition' in /components/com_content/models/frontpage.php,
		//  function _buildContentWhere
		// 1. For Articles
		$WhereState = '';
		if ($this->EditStateAllowed(0)) {
			// Content state filter
			if ( is_numeric($filter_state)) {
				$WhereState = 'ar.state = '  . (int) $filter_state ;
			}
		}
		else {
			$WhereState = 'ar.state = 1';
		}
		if (empty($WhereState)) {
			// Not trashed
			$WhereState = 'ar.state != -2';
		}
		$Where .= ' AND ' . $WhereState;
		// 2. The same - for comments:
		$WhereState = '';
		if ($this->EditStateAllowed(0)) {
			// Content state filter
			if ( is_numeric($filter_state)) {
				$WhereState = 'c.state = '  . (int) $filter_state ;
			}
		}
		else {
			$WhereState = 'c.state = 1';
		}
		if (empty($WhereState)) {
			$WhereState = 'c.state != -2';
		}
		$Where .= ' AND ' . $WhereState;

		if ($ArticleID == 0) {
			// Filter by category(ies) and even by article(s)
			$exclude = (boolean) $this->getPageValue('articlecategoryids_excludefilter', '0');
			$filter = '';
			$articlecategoryidsfilter = $this->getPageValue('articlecategoryidsfilter', '');
			if (!empty($articlecategoryidsfilter)) {
				if (!empty($filter)) {
					$filter .= ($exclude ? ' AND ' : ' OR ');
				}
				$filter .= 'ar.catid' . ($exclude ? ' NOT' : '') . ' IN(' . $articlecategoryidsfilter . ')';
			}
			$articleidsfilter = $this->getPageValue('articleidsfilter', '');
			if (!empty($articleidsfilter)) {
				if (!empty($filter)) {
					$filter .= ($exclude ? ' AND ' : ' OR ');
				}
				$filter .= 'ar.id' . ($exclude ? ' NOT' : '') . ' IN(' . $articleidsfilter . ')';
			}
			if (!empty($filter)) {
				$Where .= ' AND (' . $filter . ')';
			}
		}

		$nComments = $this->DLookup('Count(*)', $From, $Where);
			
		return $nComments;
	}

	// Find first article of the component
	function &getArticleOfComponent() {
		$article = null;

		for ($i=0; $i < $this->_nInd; $i++) {
			if ($this->_DisplayTo[$i] == 'component')
			{
				$article = & $this->_article[$i];
				break;
			}
			if ($this->_ParentOption[$i] != 'com_yvcomment' || $this->_DisplayTo[$i] == 'plugin')
			{
				$article = & $this->_article[$i];
				break;
			}
		}
		return $article;
	}

	public function getGuestID() {
		return $this->_GuestID;
	}

	function _setPage(&$params_in) {
		$mainframe = JFactory::getApplication();
		$params = new JRegistry('');

		//Set default module suffix
		$suffix = $this->_pluginParams->get('moduleclass_sfx', '');
		if ( !empty($suffix)) {
			$params->def('moduleclass_sfx', $suffix);
		}

		switch ($this->_DisplayTo[$this->_Ind])
		{
			case 'module' :
				if ($params_in->get('view_name', 'listofcomments') == 'comment') {
					// plugin in a module
					// create a copy of plugin params!
					$params->merge($this->_pluginParams);
					//echo "Merged!" ."<br/>";
				}
				$params->merge($params_in);
				break;
			case 'plugin' :
				// create a copy of plugin params!
				$params->merge($this->_pluginParams);
				$params->merge($params_in);
				break;
			case 'component' :
				// echo "ViewName=" . $this->ParentView() ."<br/>";
				if ($this->ParentView() == 'comment') {
					// plugin in component
					// create a copy of plugin params!
					$params->merge($this->_pluginParams);
					//echo "Merged!" ."<br/>";
				}
				if (!$mainframe->isAdmin()) {
					$option = 'com_yvcomment';
					// Get the page/component configuration
					$PageParameters = $mainframe->getPageParameters($option);
					$params->merge($PageParameters);
					//echo 'params1toString: <br/>' . $params->toString() . '<br/>';
				}
				break;
			default :
				$message = 'Unknown DisplayTo="' . $this->_DisplayTo[$this->_Ind] . '"';
				$mainframe->enqueueMessage($message, 'notice');
		}

		// Set some defaults
		$params->def('yvcomment_limit',0);
		$params->def('yvcomment_limitstart',0);

		$this->_PageParameters[$this->_Ind] = & $params;
	}

	function setPagination() {
		$mainframe = JFactory::getApplication();
		$message = '';
			
		if ($this->_PageParameters[$this->_Ind] && $this->_IsPaginationEnabled) {
			// Only one pagination per page - this is Joomla! restriction...
			$this->_IsPaginationEnabled = false;

			//$message .= 'limit="' . JRequest::getVar('limit', '(not set)') . '; stored=' . $this->_limit1 . '"<br/>';
			$limit = $mainframe->getUserStateFromRequest('yvcomment_limit', 'limit', $mainframe->getCfg('list_limit'));

			// ToDo; to figure out, Who's responsible for this...
			// This is very strange value at the beginning of the session, that causes wrong pagination...
			if ($limit == 9) {
				$limit = 10;
			}
			$this->_PageParameters[$this->_Ind]->set('yvcomment_limit', $limit);

			// Fixing Joomla pagination bug 2007-10-13
			//$message .= 'limitstart="' . JRequest::getVar('limitstart', '(not set)') . '"<br/>';
			//$message .= 'start="' . JRequest::getVar('start', '(not set)') . '"<br/>';

			$varName = 'limitstart';
			if (!$mainframe->isAdmin()) {
				if ((JRequest::getVar($varName) === null)) {
					$varName = 'start';
				}
			}
			$this->_PageParameters[$this->_Ind]->set('yvcomment_limitstart', $mainframe->getUserStateFromRequest('yvcomment_limitstart', $varName, '0'));

			//$message .= 'getUserStateFromRequest="'
			//  . $mainframe->getUserStateFromRequest('yvcomment_limit', 'limit', $mainframe->getCfg('list_limit'))
			//  . '; cfg=' . $mainframe->getCfg('list_limit')
			//  . '; PageValue=' . $this->getPageValue('yvcomment_limit', '0');
		}
		if (!empty($message)) {
			$mainframe->enqueueMessage($message, 'notice');
		}
	}

	function &PageParameters()
	{
		return $this->_PageParameters[$this->_Ind];
	}

	/*
	 * Returns value of Page/Component/Plugin parameter
	 * from Instance-specific attributes
	 */
	function getPageValue($paramName = '', $default = '') {
		$value = $default;

		if ($this->_PageParameters[$this->_Ind]) {
			$value = $this->_PageParameters[$this->_Ind]->get($paramName, $default);
		}
		//echo 'getPageValue param="' . $paramName . '", value="' . $value . '"<br/>';
		return $value;
	}

	function setPageValue($paramName = '', $value = '') {
		if ($this->_PageParameters[$this->_Ind]) {
			$this->_PageParameters[$this->_Ind]->set($paramName, $value);
		}
	}

	// Show List of comments for the Article (inside another view...)
	function ShowCommentsOnArticle($params_in = null) {
		$Ok = true;
		$strOut = "";
		$task = 'viewdisplay';

		$params = new JRegistry($params_in);
		$ArticleID = $this->getArticleID();
		if ($ArticleID == '0') {
			return;
		}
		$params->set('filterbycontext','article');

		$InstanceInd = $this->BeginInstance('plugin', $params);
		$this->setArticleID($ArticleID);

		$viewName = $this->getPageValue('view_name', 'listofcomments');
		$layoutName = $this->getPageValue('layout_name', 'default');
		if ($layoutName == '0') {
			$layoutName = $this->getPageValue('layout_name_custom', 'default');
		}
		JRequest::setVar('layout', $layoutName);

		$show_pagination = $this->getPageValue('show_pagination', false);
		if (!$show_pagination) {
			// Next line doesn't work, because it doesn't really set parameter to 'false':
			//   $this->setPageValue('show_pagination', false);
			// And this works:
			$this->setPageValue('show_pagination', '0');
			// echo 'show_pagination=' . $this->getPageValue('show_pagination', '???') . ';';
			$limit = intval($this->getPageValue('count', 0));
			if ($limit > 0) {
				$this->setPageValue('yvcomment_limit', $limit);
			}
		}

		if ($Ok) {
			$config = array ();
			$config['task'] = $task;
			$config['view'] = $viewName;
			$config['comment_type_id'] = $this->CommentTypeId;
				
			// This is needed only because we can't 'undefine' this:
			//define( 'JPATH_COMPONENT',					JPATH_BASE.DS.'components'.DS.$name);
			$config['base_path'] = JPATH_SITE_YVCOMMENT;

			// Create the controller
			$controller = new yvcommentController($config);

			// Perform the Request task
			$controller->execute($task);

			$strOut .= $controller->getOutput();
		}

		$this->EndInstance($InstanceInd);
		return $strOut;
	}

	// Unify the introtext and fulltext fields before passing to editor...
	// for compatibility with content plugins...
	// The only difference with Joomla! core behavior is that
	//  there is no <hr> in case introtext is empty
	//  (for compatibility with versions of yvComment prior v.1.21.0)
	function UnifyIntrotextFulltext(& $item) {
		$text = '';
		if ($item) {
			$text = $item->introtext;
			if (JString::strlen($item->fulltext) > 0) {
				if (JString::strlen($text) > 0) {
					$text = $text . "<hr id=\"system-readmore\" />";
				}
				$text = $text . $item->fulltext;
			}
		}
		return $text;
	}

	function PrepareItemForView(& $item, $replaceLinebreaks = true)
	{
		$Ok = (boolean)($item);
		if (!$Ok) {
			echo 'Item is not an object?! "' . print_r($item, true) . '"';
		} else if (!isset($item->introtext) && !isset($item->fulltext)) {
			$Ok = false;
			echo 'Item:"' . print_r($item, true) . '"';
		}
		if ($Ok) {
			$text = '';
			$params = new JRegistry($item->attribs);

			// Based on the code from the _loadArticleParams function
			//   of the "components/com_content/models/article.php" file,
			//   but it's not the same code...

			// Are we showing introtext with the comment
			$ShowIntro = true;
			if (JString::strlen($item->fulltext) > 1) {
				if (!$params->get('show_intro', true)) {
					$ShowIntro = false;
				}
			}
			if ($ShowIntro) {
				$text = $item->introtext;
			}
			if (JString::strlen($item->fulltext) > 0) {
				if (JString::strlen($text) > 0) {
					// There is intro AND fulltext in the Content
					$item->readmore_link = $this->ContentIDToURL($item->id);
					$item->readmore_register = false;
					// This is an "Alternative Read more text" article parameter
					$item->readmore_text = $params->get('readmore', '');
					if (JString::strlen($item->readmore_text) < 1) {
						$item->readmore_text = JText::sprintf('Read more...');
					}
				} else {
					$text = $text . $item->fulltext;
				}
			}
			$item->text = $text;

			if ($replaceLinebreaks && strpos( ' ' . $item->text,'<script') < 1) {
				// TODO: intellectual replace: don't replace inside script...
				$item->text = str_replace(chr(10), '<br />', $item->text);
			}
			if ($this->getConfigValue('execute_content_plugins', '0')) {
				// Model is the same in the case of recursion,
				// so don't refer to model after calling plugins!
					
				$item->event = new stdClass();
				$params = new JRegistry('');

				// Disable yvComment plugin in this view
				$params->set('yvcomment_view','none');

				$dispatcher = JDispatcher::getInstance();
				$dispatcher->trigger('onPrepareContent', array (&$item, &$params, 0));

				$results = $dispatcher->trigger('onAfterDisplayContent', array (&$item, &$params, 0));
				$item->event->afterDisplayContent = trim(implode("\n", $results));
			}
		}
	}

	// May current user add comment to this Article
	function AddAllowed($articleId) {
		$allowed = $this->ActionAllowed('core.create', $articleId);
		if ($allowed) {
			$allowed = $this->AddAllowedForArticle($articleId);
		}
		return $allowed;
	}

	/**
	 * Is addition of comments to this Article allowed based on:
	 * - article's Category
	 * - article's ID
	 * - auto close comments option
	 * - max_level_of_comments option
	 * - ..
	 */
	function AddAllowedForArticle($ArticleID_in) {
		$message = "";
		$Ok = true;

		if ($this->_debugSec) {
			echo '<div class="CommentMessage">';
		}
		if ($this->AddAllowedForArticle_ArticleID != $ArticleID_in) {
			$this->AddAllowedForArticle_ArticleID = $ArticleID_in;
			$this->AddAllowedForArticle_Enabled = null;
		}
		if 	($this->AddAllowedForArticle_Enabled == null) {
			$this->AddAllowedForArticle_Enabled = false;
			if ($this->AddAllowedForArticle_ArticleID != 0) {
				if ($this->_debugSec) { echo 'ArticleID="' . $this->AddAllowedForArticle_ArticleID . '"; '; }
				// Check if we can add comments to this article
				$articlecategoryids = trim($this->getConfigValue('articlecategoryids', ''));
				$articleids = trim($this->getConfigValue('articleids', ''));
				if ((strlen($articlecategoryids) == 0)
				&& (strlen($articleids) == 0)) {
					$this->AddAllowedForArticle_Enabled = true;
				} else {
					$blnExclude = (bool) $this->getConfigValue('articlecategoryids_exclude', '0');
					$blnFound = false;

					if (strlen($articleids) > 0) {
						// Find $this->AddAllowedForArticle_ArticleID in this list
						$array1 = explode(",", $articleids);
						foreach ($array1 as $articleid1) {
							if ((int)$articleid1 == $this->AddAllowedForArticle_ArticleID) {
								$blnFound = true;
								break;
							}
						}
						if ($this->_debugSec && $blnFound) { echo 'ArticleID found in filter; '; }
					}
					if ((!$blnFound) && (strlen($articlecategoryids) > 0)) {
						$ArticleCategoryID = $this->DLookup('catid','#__content','id=' . $this->AddAllowedForArticle_ArticleID);
						// Find $ArticleCategoryID in this list
						$array1 = explode(",", $articlecategoryids);
						foreach ($array1 as $categoryid1) {
							if ((int)$categoryid1 == $ArticleCategoryID) {
								$blnFound = true;
								break;
							}
						}
						if ($this->_debugSec && $blnFound) { echo 'CategoryID=' . $ArticleCategoryID . ' found in filter; '; }
					}

					if ($blnFound) {
						$this->AddAllowedForArticle_Enabled = !$blnExclude;
					} else {
						$this->AddAllowedForArticle_Enabled = $blnExclude;
						if ($this->_debugSec && !$blnExclude) {
							echo 'Not found in filter (CategoryID=' . ( isset($ArticleCategoryID) ? $ArticleCategoryID : '?') . '); ';
						}
					}
				}
			}
			if ($this->AddAllowedForArticle_Enabled) {
				if ($this->CommentsAreClosed($this->AddAllowedForArticle_ArticleID)) {
					$this->AddAllowedForArticle_Enabled = false;
					if ($this->_debugSec) { echo 'Comments are closed; '; }
				}
			}
			if ($this->AddAllowedForArticle_Enabled && $this->IsCommentByID($this->AddAllowedForArticle_ArticleID)) {
				// 'Article' in fact, is a Comment, so let's rename variable
				$CommentID = $this->AddAllowedForArticle_ArticleID;
				$comments_on_comment = $this->getConfigValue('allow_comments_on_comment', '0');
				switch ($comments_on_comment) {
					case 'administrators_reply_only' :
					case 'owners_reply_only' :
					case 'one_level_deep' :
						$ParentID = $this->DLookup('parentid', $this->getTableName(), 'id=' . $CommentID);
						if ($this->IsCommentByID($ParentID)) {
							// no more, than one level
							$this->AddAllowedForArticle_Enabled = false;
							if ($this->_debugSec) { echo 'No more, than one level; '; }
						} elseif (($comments_on_comment == 'owners_reply_only') ||
						($comments_on_comment == 'administrators_reply_only')	) {
							// Are there any comments already
							$ChildID = $this->DLookup('id', $this->getTableName(), 'parentid=' . $CommentID);
							if ($ChildID != 0) {
								// Only one 'Owners reply' (or any reply...) is allowed
								$this->AddAllowedForArticle_Enabled = false;
								if ($this->_debugSec) { echo 'Only one reply is allowed; '; }
							} else {
								// Is User Owner or Admin?
								// TODO: We need to decide: Who is 'Admin' here?
								if ($this->EditAllowed(0)) {
									// User is not Admin
									if ($comments_on_comment == 'administrators_reply_only') {
										$this->AddAllowedForArticle_Enabled = false;
										if ($this->_debugSec) { echo 'User is not Admin; '; }
									} else {
										$ArticleAuthorID = $this->DLookup('created_by', $this->getTableName(), 'id=' . $CommentID);
										$user		= JFactory::getUser();
										if ($user->get('id') != $ArticleAuthorID) {
											// User is not the Owner of the article (and is not Admin...)
											$this->AddAllowedForArticle_Enabled = false;
											if ($this->_debugSec) { echo 'User is not the Owner of the Article; '; }
										}
									}
								}
							}
						}
						break;
					case 'threaded_comments' :
						if ($this->IsCommentByID($this->AddAllowedForArticle_ArticleID)) {
							$max_level_of_comments = $this->getConfigValue('max_level_of_comments', '2');
							static $max_max_level_of_comments = 500;
							if ($max_level_of_comments < 2 || $max_level_of_comments > $max_max_level_of_comments) {
								static $max_level_of_comments_error_reported = false;
								if (!$max_level_of_comments_error_reported) {
									$message .= "'" . JText::_('MAX_LEVEL_OF_COMMENTS') . "' option value should be >= '2' and <=" . $max_max_level_of_comments . "<br />";
									$max_level_of_comments_error_reported = true;
								}
							} else {
								// check restrictions on max level of comments...
								$level = 0;
								$id1 = $CommentID;
								do {
									$ParentID = (int) $this->DLookup('parentid', $this->getTableName(), 'id=' . $id1);
									if (!$this->IsCommentByID($ParentID)) {
										break;
									}
									$level += 1;
									if ($level > $max_level_of_comments) {
										$this->AddAllowedForArticle_Enabled = false;
										if ($this->_debugSec) { echo 'No more, than level number ' . $max_level_of_comments . '; '; }
										break;
									}
									$id1 = $ParentID;
								} while ($this->AddAllowedForArticle_Enabled);
							}
						}
						break;
					default : // No
						$this->AddAllowedForArticle_Enabled = false;
						if ($this->_debugSec) { echo 'Comment on comment is disabled; '; }
				}
			}
		}
		if ($this->_debugSec) {
			echo ' AddAllowedForArticle="' . $this->AddAllowedForArticle_Enabled . '"; </div>';
		}
		if (strlen($message) > 0) {
			$mainframe = JFactory::getApplication();
			$message .= self::_textSignature();
			$mainframe->enqueueMessage($message, ($Ok ? 'notice' : 'error'));
		}
		return $this->AddAllowedForArticle_Enabled;
	}

	/**
	 * Is addition of comments for this User allowed?
	 * $articleId - if != 0 to add comment on this Article 
	 * 
	 * (At least, comments on some articles...)
	 */
	function AddAllowedForUser($articleId = 0) {
		$allowed = $this->ActionAllowed('core.create', $articleId);
		return $allowed;
	}

	/**
	 *  Is Current User a Guest? (i.e. not registered)
	 */
	public static function UserIsGuest() {
		return self::$_UserIsGuest;
	}

	public static function UserIsRegistered() {
		return (!self::UserIsTemp() && !self::UserIsGuest());
	}

	public static function UserIsTemp() {
		return self::$_UserIsTemp;
	}

	/**
	 * Can current User edit state of any comments?
	 * return boolean
	 */
	public function EditStateAllowed($item = null) {
		return $this->ActionAllowed('core.edit.state', $item);
	}

	/**
	 * This applies both to Articles and Comments
	 * $data - row of the article
	 * return bool view-access
	 */
	public static function ViewAllowed(& $data) {
		$yes = false;
		$user = JFactory::getUser();
		$groups = $user->getAuthorisedViewLevels();
		$category_access = null;
		if ($data && isset($data->access)) {
			$yes = in_array($data->access, $groups);
			if ($yes && isset($data->catid) && $data->catid != 0) {
				if (isset($data->category_access)) {
					$category_access = $data->category_access;
				} else {
					$category_access = self::DLookup('access', '#__categories', 'id=' . $data->catid);
				}
				$yes = in_array($category_access, $groups);
			}
		}
		return	$yes;
	}

	/**
	 *  May current user edit this Comment
	 *  $item - the Comment item, CommentID or null (to check for any comment)
	 */
	public function EditAllowed($item = null) {
		return $this->ActionAllowed('core.edit', $item);
	}

	/**
	 *  May current user edit his own Comments
	 *  $item - the Comment item, CommentID or null (to check for any comment)
	 */
	public function EditOwnAllowed($item = null) {
		return $this->ActionAllowed('core.edit.own', $item);
	}
	
	/**
	 *  May current user edit this Comment
	 *  $item - the Comment item, CommentID or null (to check for any comment)
	 */
	public function DeleteAllowed($item = null) {
		return $this->ActionAllowed('core.delete', $item);
	}

	/**
	 *  Is this User allowed to do this action with this Comment
	 *  $action - actions: 'core.create', 'core.edit', 'core.delete' ...
	 *  $item - the Comment item, CommentID (or parent Comment/Article item/Id for 'core.add') or null (to check for any comment)
	 */
	public function ActionAllowed($action, $item) {
		$done = false;
		$stored = false;
		$itemId = 0;
		// This is commentId
		$commentId = 0;
		// Parent article or comment (they are in content table)
		$parentId = 0;
		$asset = 'com_content';
		$allowed = false;
		$user = null; 
			
		if ($item != null) {
			if (isset($item->id)) {
				$itemId = (integer) $item->id;
			} else {
				$itemId = (integer) $item;
			}
		}
		if (($action == 'core.create')) {
			$parentId = $itemId;
			if ($this->UseDesignatedCategoryForComments()) {
				// In order not to create different entries for different articles
				$itemId = $this->getCategoryForComments();
			}
		}		
		if (isset($this->allowedActions[$itemId])) {
			if (isset($this->allowedActions[$itemId][$action])) {
				$allowed = ( $this->allowedActions[$itemId][$action] == 1 ? true : false);
				$done = true;
				$stored = true;
			}
		} else {
			//Initialise the element
			$this->allowedActions[$itemId] = array();
		}
		
		if (!$done) {
			$user = JFactory::getUser();
			if (($action == 'core.create')) {
				// Can this User Add comments?
				// ACL based on permission set for the Category of Comments...
				// See http://docs.joomla.org/ACL_Tutorial_for_Joomla_1.6
				// Based on /components/com_content/controllers/article.php
				//   function allowAdd
				if (self::UserIsGuest()) {
					// For guests we Lookup permissions of special Guest User
					// We may have different impersonators of guests for different types of comments
					$user = JFactory::getUser($this->getGuestID());
				}
			} else {
				$commentId = $itemId;
			}
			if ($commentId != 0) {
				if ($this->UseContentTable() && !$this->IsCommentByID($commentId)) {
					// 2010-04-24 This is not comment, so it can not be edited etc.
					// by this Type of Comments
					$done = true;
				}
			}
		}
		
		if (!$done) {
			if (($commentId != 0) && $this->UseContentTable()) {
				// ACL-based approach
				// Permissions set on Comment's Category are used in a case
				// there are no permissions on this Comment

				// Based on the code in:
				//   '/components/com_content/controllers/article.php'
				//   function allowEdit
				$asset .= '.article.' . $commentId;
				
			} else {
				// commentId is unknown or Comments are not in the Content table, so we don't have 'article' permissions
				// (And there may be other 'Real' article with this ID !)
				// So we will use permissions for Category of comments
				if ($this->UseDesignatedCategoryForComments()) {
					$categoryId	= $this->getCategoryForComments();
					$asset .= '.category.'.$categoryId;
				} else {
					if ($parentId != 0) {
						// Use Category of parent article
						$categoryId	= $parentId;
						$asset .= '.category.'.$categoryId;
					}
					// In the absense of better information, revert to the component permissions.
					// See http://docs.joomla.org/How_to_implement_actions_in_your_code
				}
				
			}
			if ($user) {
				// Check general action permission first.
				$allowed = $user->authorise($action, $asset);
				if (!$allowed) {
					if ($action == 'core.edit' && ($commentId !=0)) {
						// Fallback on edit.own.
						// First test if the permission is available.
						if ($user->authorise('core.edit.own', $asset)) {
							if (isset($item->created_by)) {
								$created_by = $item->created_by;
							} else {
								$created_by = $this->DLookup('created_by', $this->getTableName(), 'id=' . $commentId);
							}
							if (($user->get('id')) == $created_by) {
								$allowed = true;
							}
						}
					}
				}
			}
		}

		if ($this->_debugSec) {
			echo '<div class="CommentMessage">' . ($allowed ? '' : 'Not ') . 'Allowed: "' . $action . '"' 
			. ($stored ? '; stored' : '; asset="' . $asset	. '"' )
			. '; ItemID=' . $itemId
			. '; type=' . $this->CommentTypeId 
			. '</div>';
		}

		if(!$stored) {
			$stored = true;
			$this->allowedActions[$itemId][$action] = ($allowed ? 1 : 2);
			// echo 'Allowed Actions=' . print_r($this->allowedActions, true) . '<br />';
		}
		return $allowed;
	}
	
	// Are comments for this Article closed?
	// (If they are not closed, this yet doesn't mean, that adding comments is allowed)
	function CommentsAreClosed($ArticleID_in) {
		static $ArticleID = null;
		static $Closed = null;

		if ($ArticleID != $ArticleID_in) {
			$ArticleID = $ArticleID_in;
			$Closed = null;
		}
		if 	($Closed == null) {
			$Closed = false;
			$auto_close_days = trim($this->getConfigValue('auto_close_days', '0'));
			if ($auto_close_days > 0) {
				// auto close comments option is activated
				jimport('joomla.utilities.date');
				$ArticleID2 = $ArticleID; // copy to work with in the loop
				do {
					// Loop to the Article itself (or first old comment...)
					$ArticleCreated = new JDate($this->DLookup('created','#__content','id=' . $ArticleID2));
					$AutoCloseTime = $ArticleCreated->toUnix() + ($auto_close_days * 24 * 60 * 60);
					if ( $AutoCloseTime < time()) {
						$Closed = true;
						break;
					}
					if (!$this->IsCommentByID($ArticleID2)) {
						break;
					}
					// Let's check parent
					$ArticleID2 = $this->DLookup('parentid','#__content','id=' . $ArticleID2);
				} while (true);
				// echo 'id="' . $ArticleID . '; "AutoCloseTime="' . $AutoCloseTime . '", time="' . time() .'"; days left="' . ($AutoCloseTime - time())/(24 * 60 * 60) . '"' . ($Closed ? ' Closed' : ' Opened') . '<br/>';
			}
		}
		return $Closed;
	}

	function getSiteURL() {
		$mainframe = JFactory::getApplication();
		$uri = JFactory::getURI();
		return ($mainframe->isAdmin() ? $uri->root() : JURI :: base());
	}

	/**
	 *	Compare version of some (yvComment) extension (plugin, module...)
	 *	with the version of the yvCommentComponent
	 */
	public static function VersionChecks($ExtensionName='', $ExtensionVersion='', $AlwaysWarn = false) {
		$Ok = (self::JoomlaCoreVersionCheck($AlwaysWarn) && self::OkAll());
		if ($Ok && !empty($ExtensionVersion)) {
			$Ok = (strcmp($ExtensionVersion, self::getCompatibleVersion()) == 0);
			if (!$Ok) {
				$mainframe = JFactory::getApplication();
				$mainframe->enqueueMessage(
					'Versions of "' . $ExtensionName . '" and "yvComment Component" are not the same.<br/>' 
					. '(' . $ExtensionName . ' version="' . $ExtensionVersion . '"; Component version="' . $this->getCompatibleVersion() . '")<br/>'
					. 'Please install the same versions from <a href="http://yurivolkov.com/Joomla/yvComment/index_en.html" target="_blank">yvComment home page</a>.',
					'error');
					self::HideAll();
			}
		}
		return $Ok;
	}

	// Joomla! core version check
	// Returns Ok
	public static function JoomlaCoreVersionCheck($AlwaysWarn = false) {
		static $stOk = null;  // not less, than $ver_min
		$mainframe = JFactory::getApplication();
		if (is_null($stOk)) {
			$ver_Ok		= '2.5.0';
			$ver_min	= '1.6.0';
			$ver_min_name	= 'Joomla! version ' . $ver_min;

			$stOk = false;
			$message = '';
			$Warning = false;
			$Error = true;
			$ver = 0;

			$versionName = 'Very old (Doesn\'t have JVersion Class)';
			$ver = self::JoomlaShortVersion();
			if (version_compare( $ver, '1.5.1' ) < 0) {
				$Error = true;
			} else {
				$versionName = 'Joomla! version ' . $ver;
			}
			if (version_compare( $ver, $ver_min ) >= 0) {
				$Error = false;
				$stOk = true;
				if (version_compare( $ver, $ver_Ok ) < 0) {
					if ($AlwaysWarn) {
						$Warning = true;
					} else {
						$Warning = (boolean) self::getInstance()->getConfigValue('joomla_version_warning', '1');
					}
				}
			}
			if ($Error || $Warning){
				if (version_compare( $ver, '0.0.0' ) == 0) {
					$message .= JText::_( 'JOOMLA_VERSION_IS_UNKNOWN') . '<br/>';
					$message .= str_replace( '%1', 'Joomla! version ' . $ver_Ok, JText::_( 'JOOMLA_VERSION_WARNING_MESSAGE')) . '<br/>';
				} elseif ($Warning) {
					//Warning! Your version of Joomla is not up-to-date...
					$message .= JText::_( 'JOOMLA_VERSION_IS_NOT_UPTODATE') . '<br/>';
					$message .= str_replace( '%1', 'Joomla! version ' . $ver_Ok, JText::_( 'JOOMLA_VERSION_WARNING_MESSAGE')) . '<br/>';
				} else {
					$message .= str_replace( '%1', $ver_min_name, str_replace( '%2', $ver_Ok, JText::_( 'JOOMLA_VERSION_ERROR'))) . '<br/>';
				}
				if (version_compare( $ver, '0.0.0' ) > 0) {
					$message .= str_replace( '%1', $versionName, JText::_( 'CURRENT_VERSION_OF_JOOMLA')) . '<br/>';
				}
				$message .= self::_textSignature();
			}
			if (!$stOk) {
				$this->_Ok = false;
			}
			if (!$stOk) {
				$mainframe->enqueueMessage($message, 'error');
				return false;
			} elseif (strlen($message) > 0) {
				$mainframe->enqueueMessage($message, 'notice');
			}
		}
		return $stOk;
	}

	public static function JoomlaShortVersion(){
		jimport('joomla.version');
		$ShortVersion = '0.0.0';
		$version = new JVersion();
		if ($version) {
			$ShortVersion = $version->getShortVersion();
			//echo 'Build="' . $version->BUILD . '"<br>';
			//echo 'LongVersion="' . $version->getLongVersion() . '"<br>';
			//echo 'ShortVersion="' . $version->getShortVersion() . '"<br>';
		}
		return $ShortVersion;
	}

	// Terminology just like in JVersion class
	public static function getShortVersion() {
		return self::$_Release .'.'. self::$_Dev_level;
	}
	// To compare versions of different components for compatibility
	public static function getCompatibleVersion() {
		return self::$_Release;
	}

	// Credits for install/uninstall
	public static function Credits() {
		include 'assets' . DS . 'credits.php';
	}

	/**
	 * Get the value from the comment metadata, which is an INI formatted string.
	 * Empty input will return null.
	 */
	function getValueFromIni($metadata_str, $key = '')
	{
		if(empty($metadata_str)) {
			return null;
		}

		$metadata_registry = new JRegistry();
		$metadata_registry->loadJSON($metadata_str);
		$value = $metadata_registry->getValue($key);
		switch ($key) {
			case 'created_by_link':
			case 'created_by_email':
				$value = urldecode(html_entity_decode($value));
				break;

		}
		//echo '"' .$key . '"-"' . $value . '"<br />';
		return $value;
	}

	/**
	 * Convert an arbitrary string into an XHTML-safe HREF that can be used as a link.
	 * Used to allow users to enter a link back to their site.  No additional quoting is
	 * required, it is XHTML safe.
	 *
	 * The username and password is not preserved in the URL to prevent users from
	 * inadvertently giving away a sensitive login.
	 */
	function strToSafeHref($str)
	{
		$debug = false;

		// First step is to remove any obvious attempts at cross site scripting. This will
		// inadvertently catch some valid URLs, but it's a small price to pay for the
		// additional safety.
		$str = preg_replace('/script:/', '', $str);

		// If there is no host there may be a missing scheme.
		$uri = new JURI(trim($str));
		if(!$uri->getHost()) {
			$uri = new JURI('http://'.trim($str));
		}

		if($debug) {
			echo "URI = $str\n";
			echo "\tURI scheme = ".$uri->getScheme(), "\n";
			echo "\tURI host = ".$uri->getHost(), "\n";
			echo "\tURI port = ".$uri->getPort(), "\n";
			echo "\tURI path = ".$uri->getPath(), "\n";
			echo "\tURI query = ".$uri->getQuery(), "\n";
			echo "\tURI fragment = ".$uri->getFragment(), "\n";
		}

		// Only allow http or https as the protocol
		$scheme = 'http';
		if($uri->getScheme()) {
			$scheme = strtolower($uri->getScheme());
		}
		if($scheme != 'http' && $scheme != 'https' ) {
			$scheme = 'http';
		}
		$result = $scheme.'://';

		// There must be a host.
		$host = $uri->getHost();
		$filter = JFilterInput::getInstance();
		if(!empty($host)) {
			$host = $filter->clean($host, 'string');
		}
		if(empty($host)) {
			return null;
		}
		$result .= urlencode(htmlentities($host));

		// Port number must be an integer.
		$port = $uri->getPort();
		if(!empty($port)) {
			if(!is_numeric($port) || (intval($port) != $port)) {
				return null;
			} else {
				$port = intval($port);
				$result .= ':'.$port;
			}
		}

		// Path without a leading slash.
		$path = $uri->getPath();
		if(!empty($path)) {
			$path = preg_replace('/^[\\\\\/]/', '', $path);
			$components = preg_split('/^[\\\\\/]/', $path);
			foreach($components as $component) {
				$component = $filter->clean($component, 'string');
			}
			$path = join('/', array_map('urlencode', array_map('htmlentities', $components)));
			$result .= '/'.$path;
		}

		// Query.  Clean each name and value pair.
		$query = $uri->getQuery();
		if(!empty($query)) {
			$first_pair = true;
			$pairs = preg_split('/&/', $query);
			foreach($pairs as $pair) {
				$components = preg_split('/[=]/', $pair);
				foreach($components as $component) {
					$component = $filter->clean($component, 'string');
				}

				$result .= $first_pair ? '?' : '&amp;';
				$result .= join('=', array_map('urlencode', array_map('htmlentities', $components)));
				$first_pair = false;
			}
		}

		// Fragment
		$fragment = $uri->getFragment();
		$fragment = $filter->clean($fragment, 'string');
		if(!empty($fragment)) {
			$result .= '#'.urlencode(htmlentities($fragment));
		}

		if($debug) {
			echo "\tResult = $result\n";
		}
		return $result;
	}

	/*
	 // Test cases for strToSafeHref.
	 $list = array(
	 'http://www.example.com/valid/path/to.cgi?with&arg=42#and-fragment'
		, 'example.com'
		, 'HTTP://www.example.com'
		, 'http://foo/javascript:/inpath'
		, 'http://javascript:/gotcha'
		, 'javascript://www.example.com'
		, "java\nscript://www.example.com"
		, 'http://javascript:alert("Opps")'
		, 'http:///nohost'
		, 'http://backslashes/c:\\autoexec.bat'
		, 'http://host.with/42<6x9'
		);
		echo '<pre>';
		foreach( $list as $str ) {
		$safeHref = strToSafeHref($str);
		echo '<a href="', $safeHref, '">', htmlentities($str), "</a><br />\n";
		}
		echo '</pre>';
		*/

	// TODO: Smart trim words of plain text
	public static function TrimText($text_in, $maxlength, $more='')
	{
		$strOut = JString::trim(strip_tags($text_in));
		if(($maxlength > 0) && (JString::strlen($strOut) > $maxlength)) {
			if ($more=='') $more = "&hellip;";
			$strOut = JString::substr($strOut, 0, $maxlength) . $more;
		}
		return $strOut;
	}

	// Signature of this Extension
	private static function _textSignature() {
		$message = '<br/>-- <br/>' .
		'<a href="http://yurivolkov.com/Joomla/yvComment/index_en.html" target="_blank">' . 
		'yvComment solution, version="' . 
		self::getShortVersion() . '"</a>';
		return $message;
	}

	function log($message) {
		error_log($message);
		//syslog(LOG_INFO, $message);

		//$mainframe = JFactory::getApplication();
		//if (is_object($mainframe)) {
		//	$mainframe->enqueueMessage($message, 'notice');
		//}
	}

	function CommentIDToURL($id_in, $pathonly = true, $UserID = null, $ShowUnpublished = false) {
		$url = null;
		if ($this->UseContentTable()) {
			$url = self::ContentIDToURL($id_in, $pathonly, $UserID, $ShowUnpublished);
		}
		return $url;
	}

	/* Build URL to the Article by it's ID, taking SEF settings,
	 * security for UserID etc. into account
	 * @version		ContentIDToURL 011 2011-06-13 00:00:00Z yvolk
	 * @author	  yvolk (Yuri Volkov) <http://yurivolkov.com>
	 * Based on code from getList function of 'modules/mod_latestnews/helper.php'
	 * It is very strange, that Joomla didn't have such function yet...
	 * It has now :-), but the implementation is far from ideal...
	 *
	 * v.009 Optionally $fragment may be added to the URL
	 * v.010 Modified for Joomla! 1.6
	 * v.011 badcats error fixed (when $ShowUnpublished)
	 */
	static function ContentIDToURL($id_in, $pathonly = true, $UserID = null, $ShowUnpublished = false, $fragment = "") {
		$debug = false;
		//$debug = ($id_in == 21);
		$message = '';
		$id = (integer) $id_in;
		$url = '';
		if ($id > 0) {
			$mainframe = JFactory::getApplication();

			$db		= JFactory::getDBO();
			$user	= JFactory::getUser($UserID);
			$groups = $user->getAuthorisedViewLevels();

			$contentConfig = JComponentHelper::getParams( 'com_content' );
			$access		= !$contentConfig->get('shownoauth');
			$authorised = !$access;
			$where = '';
			if ($debug) {
				$message .= 'articleid=' . $id_in . '<br/>';
			}

			// Just like it is done in 'components/com_content/models/article.php'
			$query = $db->getQuery(true);

			// If badcats is not null, this means that the article is inside an unpublished category
			// In this case, the state is set to 0 to indicate Unpublished (even if the article state is Published)
			$query->select('a.id, a.alias, ' .
				($ShowUnpublished ? 'a.state, ' : 'CASE WHEN badcats.id is null THEN a.state ELSE 0 END AS state, ') .
				'a.mask, a.catid, ' .
				'a.publish_up, a.publish_down, ' .
				'a.parentid, ' .
				'a.access' );
			$query->from('#__content AS a');

			// Join on category table.
			$query->select('c.alias AS category_alias, c.access AS category_access');
			$query->join('LEFT', '#__categories AS c on c.id = a.catid');

			// Join over the categories to get parent category titles
			$query->select('parent.id as parent_id, parent.alias as parent_alias');
			$query->join('LEFT', '#__categories as parent ON parent.id = c.parent_id');

			$query->where('a.id = ' . (int) $id);

			if (!$ShowUnpublished) {
				// Links to the archived content are enabled, so state = -1
				$query->where('a.state IN(-1, 1)');

				// Filter by start and end dates.
				$nullDate = $db->Quote($db->getNullDate());
				$nowDate = $db->Quote(JFactory::getDate()->toMySQL());

				$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')');
				$query->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');

				// Join to check for category published state in parent categories up the tree
				// If all categories are published, badcats.id will be null, and we just use the article state
				$subquery = ' (SELECT cat.id as id FROM #__categories AS cat JOIN #__categories AS parent ';
				$subquery .= 'ON cat.lft BETWEEN parent.lft AND parent.rgt ';
				$subquery .= 'WHERE parent.extension = ' . $db->quote('com_content');
				$subquery .= ' AND parent.published <= 0 GROUP BY cat.id)';
				$query->join('LEFT OUTER', $subquery . ' AS badcats ON badcats.id = c.id');
			}

			$db->setQuery($query);

			$item = $db->loadObject();
			if (!is_null($item)) {

				if (!$authorised) {
					if ($item->catid == 0 || $item->category_access === null) {
						$authorised = in_array($item->access, $groups);
					}
					else {
						$authorised = in_array($item->access, $groups) && in_array($item->category_access, $groups);
					}
				}
				if ($authorised) {
					if ($mainframe->isAdmin()) {
						// TODO: 2011-02-11 yvolk I couldn't find any example of how to create a link
						// to the article preview from backend, only to the 'edit' task :-(
						// like in '/administrator/components/com_content/views/featured/tmpl/default.php':
						$route = 'index.php?option=com_content&task=article.edit&return=featured&id='.$item->id;
						if (strlen($fragment) > 0) {
							$route .= '#' . $fragment;
						}
						$url = JRoute::_($route);
						if ($debug) {
							$message .= 'isAdmin, url_1=\'' . $url . '\'<br/>';
						}
					} else {
						// Build URL
						// Just like it is done in 'components/com_content/views/article/view.html.php'

						// Add router helpers.
						$item->slug			= $item->alias ? ($item->id.':'.$item->alias) : $item->id;
						$item->catslug		= $item->category_alias ? ($item->catid.':'.$item->category_alias) : $item->catid;
						$item->parent_slug	= $item->category_alias ? ($item->parent_id.':'.$item->parent_alias) : $item->parent_id;

						// TODO: Change based on shownoauth
						$route = ContentHelperRoute::getArticleRoute($item->slug, $item->catslug);
						if (strlen($fragment) > 0) {
							$route .= '#' . $fragment;
						}
						$url = JRoute::_($route);
						if ($debug) {
							$message .= 'slug=' . $item->slug . '<br/>';
							$message .= 'url_1=\'' . $url . '\'<br/>';
						}

						// This hack is needed for the backend.
						// Oherwise the link would lead to the Article manager...
						if ( (!$pathonly)
						|| (substr($url, 0, 1) != '/')
						) {
							// Code from JURI::root()
							$uri =& JURI::getInstance(JURI::base());
							$root = array();
							$root['prefix'] = $uri->toString( array('scheme', 'host', 'port') );
							$root['path']   = rtrim($uri->toString( array('path') ), '/\\');

							if (substr($url, 0, 1) != '/') {
								// Maybe this will not be executed...
								// Do we need 'path' of the 'base' here?
								// $url = $root['path'] . '/' . $url;
								$url = '/' . $url;
							}
							if (!$pathonly) {
								$url = $root['prefix'] . $url;
							}
							if ($debug) {
								$message .= 'prefix=\'' . $root['prefix'] . '\'<br/>';
								$message .= 'path=\'' . $root['path'] . '\'<br/>';
							}
						}
					}
					if (strlen($fragment) > 0) {
						// 2009-07-11 There is Joomla! bug in v.1.5.12:
						// JURI class remembers the 'fragment' of the processed URI and uses it
						// in subsequent calls to build URIs...
						// So let's clear this fragment
						$uri = JFactory::getURI();
						$uri->setFragment('');
					}
				} else {
					if ($debug) {
						$message .= 'access denied<br/>';
					}
				}
				if ($access && $debug) {
					$message .= '$item->access="' . $item->access . '";';
					if ($item->catid != 0 && $item->category_access !== null) {
						$message .= '$item->category_access="' . $item->category_access . '";';
					}
					$message .=	'groups="' . print_r($groups, true) . '";'
					. '<br/>';
				}
			}	else {
				if ($debug) {
					$message .= 'row is null<br/>';
					if (function_exists('printDbErrors')) {
						$message .= self::printDbErrors($db);
					} else {
						$message .= ';DB error: "' . $db->getErrorMsg(false) . '"';
					}
				}
			}
		}
		if ($debug) {
			$message .= 'url_3=\'' . $url . '\'<br/>';
		}
		if ($message) {
			//$mainframe = JFactory::getApplication();
			//$mainframe->enqueueMessage($message, 'notice');
			$url .= '?DEBUGMSGSTART' . $message . 'DEBUGMSGEND';
		}
		return $url;
	}

	// Template helper function to create HTML elements to mention Author
	// and, optionally, link to the Author
	function htmlAuthorName(&$ViewObj, &$item) {
		//echo print_r($ViewObj, true);
		$link = '';

		$SpanClass = '';
		$AuthorName = '';
		$url = '';
		$IsExternalLink = false;

		if ($item->created_by == $this->getGuestID()) {
			$created_by_username = $this->getValueFromIni($item->metadata, 'created_by_username');
			if ($created_by_username) {
				$SpanClass = 'CommentAuthorOpenID';
				$AuthorName = $item->created_by_alias;
			} else {
				$SpanClass = 'CommentAuthorAlias';
				$AuthorName = $item->created_by_alias;
			}
		} else {
			$SpanClass = 'CommentAuthorName';
			if (isset($item->AuthorName)) {
				$AuthorName = $item->AuthorName;
			} else {
				$author_mentioned_by = $this->getConfigValue('author_mentioned_by', 'name');
				$AuthorName = $this->DLookup($author_mentioned_by, '#__users', 'id=' . $item->created_by);
			}
		}

		if ($ViewObj->params->get('author_linkable')) {
			// If Author is Guest
			if ($item->created_by == $this->getGuestID()) {
				// For Guests only:
				$url = $this->getValueFromIni($item->metadata, 'created_by_link');
				if(empty($url)) {
					$url = $this->getValueFromIni($item->metadata, 'created_by_username');
				}
				if(empty($url)) {
					$url = $this->getValueFromIni($item->metadata, 'created_by_email');
					if(!empty($url)) {
						$url = 'mailto:' . $url;
					}
				}
				if(!empty($url)) {
					$IsExternalLink = true;
				}
			} else switch ($ViewObj->params->get('author_linkable')){
				case 'link_to_the_cb_profile':
					$url = JRoute::_('index.php?option=com_comprofiler&task=userprofile&user=' . $item->created_by);
					$pathonly = false;
					// This hack is needed for the backend.
					// Oherwise the link would lead to the CB Admin root (Article manager for articles)...
					if ( (!$pathonly)
					|| (substr($url, 0, 1) != '/')
					) {
						if (substr($url, 0, 1) != '/') {
							// Maybe this will not be executed...
							// Do we need 'path' of the 'base' here?
							// $url = $root['path'] . '/' . $url;
							$url = '/' . $url;
						}
						if (!$pathonly) {
							// Code from JURI::root()
							$uri =& JURI::getInstance(JURI::base());
							$root['prefix'] = $uri->toString( array('scheme', 'host', 'port') );
							$url = $root['prefix'] . $url;
						}
					}
					break;
				default:
					//echo print_r($item, true);
					if (isset($item->webpage)) {
						$url = $item->webpage;
					}
					if(!empty($url)) {
						$IsExternalLink = true;
					}
			}
		}

		// Everything is ready, so let's build the link
		$link = $AuthorName;
		if(!empty($link)) {
			if(!empty($url)) {
				$link = '<a href="' . $url . '"' . ($IsExternalLink ? ' target="_blank"' : '') . '>'
				. $link
				. '</a>';
			}
			if(!empty($SpanClass)) {
				$link = '<span class="' . $SpanClass . '">'
				. $link
				. '</span>';
			}
		}
		return $link;
	}

	// Build text 'to mention Author' for this content table row (item...)
	function txtAuthorName(&$item) {
		$AuthorName = '';
		if (is_object($item) && isset($item->created_by)) {
			if ($item->created_by == $this->getGuestID()) {
				$created_by_username = $this->getValueFromIni($item->metadata, 'created_by_username');
				if ($created_by_username) {
					$AuthorName = $item->created_by_alias;
				} else {
					$AuthorName = $item->created_by_alias;
				}

				$details = '';
				$part = $this->getValueFromIni($item->metadata, 'created_by_username');
				if (!empty ($part) && (!JString::stristr($AuthorName . $details, $part))) {
					if (!empty ($details)) {
						$details .= ' , ';
					}
					$details .= $part;
				}
				$part = $this->getValueFromIni($item->metadata, 'created_by_link');
				if (!empty ($part) && (!JString::stristr($AuthorName . $details, $part))) {
					if (!empty ($details)) {
						$details .= ' , ';
					}
					$details .= $part;
				}
				$part = $this->getValueFromIni($item->metadata, 'created_by_email');
				if (!empty ($part) && (!JString::stristr($AuthorName . $details, $part))) {
					if (!empty ($details)) {
						$details .= ' , ';
					}
					$details .= $part;
				}
				if (!empty ($details)) {
					//$AuthorName .= ' ( ' . $details . ' )';
					$AuthorName .= ' , ' . $details;
				}
			} else {
				// Not guest
				if (isset($item->AuthorName)) {
					$AuthorName = $item->AuthorName;
				} else {
					$author_mentioned_by = $this->getConfigValue('author_mentioned_by', 'name');
					$AuthorName = $this->DLookup($author_mentioned_by, '#__users', 'id=' . $item->created_by);
				}
			}
		}
		return $AuthorName;
	}

	/**
	 *  Database helper functions
	 *  I didn't put them inside yvCommentHelper to reduce calling code
	 */
	public static function DLookup($Expression, $Domain, $Criteria) {
		$db = JFactory::getDBO();
		return self::DLookup_db($db, $Expression, $Domain, $Criteria);
	}

	public static function DLookup_db(&$db, $Expression, $Domain, $Criteria = '') {
		$debug = false;
		$sOut = null;
		$query = '';
		if (strpos($Expression, 'SELECT') === 0) {
			// this is already SQL statement, just execute it
			$query = $Expression;
		} else {
			$query = 'SELECT';
			$query .=  ' (' . $Expression . ') As Expr1';
			$query .= ' FROM ' . $Domain;
			if ($Criteria) {
				$query .= ' WHERE (' . $Criteria . ')';
			}
		}
		$row = null;
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($db->getErrorNum() > 0) {
			echo self::printDbErrors($db);
		}
			
		if (is_object($row))	{
			if ($debug) {
				echo get_class($row). '<br />';
				echo 'object_vars=' . '<br />';
				foreach(get_object_vars($row) as $key=>$val) {
					echo $key . '="' . $val . '"<br />';
				}
				echo 'query="' . $query . '"<br />';
			}
			$sOut = $row->Expr1;
		}
		return $sOut;
	}

	public static function TableExists($pattern, $prefix='#__') {
		$db = JFactory::getDBO();
		return self::TableExists_db($db, $pattern, $prefix);
	}

	public static function TableExists_db(&$db, $pattern, $prefix='#__') {
		$Exists = false;
		$debug = false;
		// JDatabase class doesn't substitute prefix if table name is in single (or double) quotes
		$pattern2 = str_replace( $prefix, $db->getPrefix(), $pattern);
		// '_' is a literal and not part of the pattern
		$pattern2 = str_replace( '_', '\_', $pattern2);
		$query = 'SHOW TABLES LIKE \'' . $pattern2 . '\'';
		if ($debug) echo 'query="' . $query . '"<br />';;
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($db->getErrorNum() == 0) {
			if (is_object($row))	{
				if ($debug) {
					$ind1=1;
					foreach(get_object_vars($row) as $key=>$val) {
						echo $ind1 . '. "'. $key . '"="' . $val . '"<br />';
						$ind1 +=1;
					}
				}
				$Exists = true;
			}
		} elseif ($debug) echo self::printDbErrors($db);
		if ($debug) echo ($Exists? 'true' : 'false') . '<br/>';
		return $Exists;
	}

	public static function printDbErrors($db) {
		$out = null;
		if ($db->getErrorNum() > 0)	{
			$out = '<p>Database errors:' . '<br />';
			$errors[] = array('msg' => $db->getErrorMsg(), 'sql' => $db->_sql);
			foreach( $errors as $error) {
				$out .= 'Error message: "' . $error['msg'] . '"<br />';
				$out .= '  SQL="' . $error['sql'] . '"<br /><hr />';
			}
			$out .= '</p>';
		}
		return $out;
	}

	// Return number of days, 0 - all
	function ResultDaysToNDays($result_days = 'all') {
		$nDays = 0;
		// These numbers were taken from phpbb3 code (for consistency...)
		//$limit_days	= array(0 => $user->lang['ALL_RESULTS'], 1 => $user->lang['S1_DAY'],
		//  7 => $user->lang['S7_DAYS'], 14 => $user->lang['S2_WEEKS'], 30 => $user->lang['S1_MONTH'],
		//  90 => $user->lang['S3_MONTHS'], 180 => $user->lang['S6_MONTHS'], 365 => $user->lang['S1_YEAR']);
		switch ($result_days) {
			case '1day':
				$nDays = 1;
				break;
			case '7days':
				$nDays = 7;
				break;
			case '2weeks':
				$nDays = 14;
				break;
			case '1month':
				$nDays = 30;
				break;
			case '3months':
				$nDays = 90;
				break;
			case '6months':
				$nDays = 180;
				break;
			case '1year':
				$nDays = 365;
				break;
		}
		return $nDays;
	}

	function DaysToSeconds($nDays = 0) {
		$nSeconds =  $nDays * 24 * 60 * 60;
		return $nSeconds;
	}

	// Calculate date and time $nSeconds to the past
	// Returns quoted date to be used in SQL satements
	function SecondsFromNowToSQLDate($nSeconds = 0) {
		$sqlDateQuoted = '';
		$db = JFactory::getDBO();

		jimport('joomla.utilities.date');
		$date1 = new JDate();
		//echo 'before: ' . $date1->toISO8601();
		$str1 = '-0' . $nSeconds . ' seconds';
		$date1->modify($str1);
		//echo 'after: ' . $str1 . ': ' . $date1->toISO8601() . '<br />';
		// For PHP 5.3+ only:
		//$date1->sub(new DateInterval('PT0' . $nSeconds . 'S'));
		$date1Sql = $date1->toMysql();
		$sqlDateQuoted = $db->Quote($date1Sql);
			
		return $sqlDateQuoted;
	}
}
// End of yvCommentHelper class

class yvCommentPlugin extends JPlugin {
	protected static $hide = true;
	// Should be set to > 0
	protected $CommentTypeId = 0;

	// Normal positions:
	//		'InsideBox', 'AfterContent', 'DefinedByArticleTemplate'
	// Error conditions:
	//		'DifferentVersions', 'NoComponent'
	var $_PluginPlace = 'NoComponent';

	/**
	 * Constructor
	 */
	public function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		$mainframe = JFactory::getApplication();
		//echo 'yvcomment Plugin constructor, subject="' . $subject->toString() . '"<br/>';
		if (self::$hide) {
				
		} elseif (class_exists('yvCommentHelper')) {
			$this->CommentTypeId = intval($config['comment_type_id']);
			$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);
			if ($yvComment && $yvComment->Ok()) {
				// Should we hide yvComment from this view?
				$paramName = 'comments_position';
				$view = JRequest::getCmd('view');
				switch($view) {
					case 'article':
						$paramName = 'comments_position_article_view';
						break;
					case 'frontpage':
						$paramName = 'comments_position_frontpage';
						break;
				}
				$PluginPlace = $yvComment->getConfigValue($paramName, 'AfterContent');

				// Show comments on print page of the article, even if it set to 'hide'
				if ($view == 'article' && $PluginPlace == 'hide') {
					$print = JRequest::getBool('print');
					if ($print) {
						$PluginPlace = 'AfterContent';
					}
				}

				$this->_PluginPlace = $PluginPlace;
			}
		}
		//echo 'position="' . $this->_PluginPlace . '"<br/>';
	}

	/*
	 * We don't use "onContentPrepare" here in Joomla! 1.6,
	 * because "onContentPrepare" is not called from "blog" views in Joomla! 1.6
	 * See events triggered in e.g. "components/com_content/views/featured/view.html.php"
	 * See results usage (item->event->...) in e.g. "components/com_content/views/article/tmpl/default.php"
	 * Since the article and related parameters are passed by reference,
	 * event handlers can modify them prior to display
	 */
	public function onContentBeforeDisplay($context, & $article, & $params, $page = 0) {
		//echo 'yvcomment onContentBeforeDisplay type=' . $this->CommentTypeId . '; place="' . $this->_PluginPlace . '", hide=' . (self::$hide ? 'yes' : 'no') . ', isset(article->catid))=' . (isset($article->catid) ? 'yes' : 'no') . '<br/>';
		$results = '';
		if (!self::$hide) {
			if (isset($article->catid)) {
				//$article->text .= '<hr>test line<br/>';
				//echo '<hr>Article text 1 ="' . $article->text . '"<br/>';

				switch ($this->_PluginPlace) {
					case 'hide' :
					case 'InsideBox' :
					case 'DefinedByArticleTemplate' :
						$results = $this->_PluginFunction($article, $params, $page);
						break;
				}
			}
		}
		return $results;
	}

	// Information that should be placed immediately after the generated content
	public function onContentAfterDisplay($context, & $article, & $params, $page = 0) {
		//echo 'yvcomment onContentAfterDisplay type=' . $this->CommentTypeId . '; place="' . $this->_PluginPlace . '", hide=' . (self::$hide ? 'yes' : 'no') . ', isset(article->catid))=' . (isset($article->catid) ? 'yes' : 'no') . '<br/>';
		$results = '';
		if (!self::$hide) {
			if (isset($article->catid)) {
				switch ($this->_PluginPlace) {
					case 'AfterContent' :
						$results = $this->_PluginFunction($article, $params, $page);
						break;
				}
			}
		}
		return $results;
	}

	private function _PluginFunction(& $article, & $params, $page = 0) {
		static $level = 0;
		$retval = null;
		$yvComment = &yvCommentHelper::getInstance($this->CommentTypeId);

		$InstanceInd = -1;
		$strOut = '';
		//echo '_PluginFunction: ' . print_r($article, true) . '<br />';

		// Hide this plugin for known incompatible components
		$option = JRequest::getCmd('option');
		$IncompatibleComponents = array(
			'com_incompatiblewithyvcomment', 
			'com_someotherincompatible' );
		if (in_array($option, $IncompatibleComponents)) {
			echo 'The component "' . $option . '" is incompatible with yvComment plugin';
			return $retval;
		}

		$ArticleID = 0;
		if (is_object( $article )) {
			if (isset( $article->id ))
			{
				$ArticleID = intval($article->id);
			}
		}
		//$strOut .= '<p>_PluginFunction PluginPlace="' . $this->_PluginPlace . '"; ArticleID=' . $ArticleID . '</p>';

		$viewName = '';
		if (is_object( $params )) {
			$viewName = $params->get('yvcomment_view','comment');
		}
		//$strOut .= ', viewname="' . $viewName . '"';
		if ($viewName == 'none') {
			return $retval;
		}

		// For now, only one view for plugin
		//$viewName = 'comment';

		if ($ArticleID == 0) {
			//$mainframe = JFactory::getApplication();
			//$message = 'yvComment plugin was called with this:<br />' . print_r($article, true);
			//$mainframe->enqueueMessage($message, 'notice');
			return $retval;
		}
		if ($level > 1) {
			// Do we need to make yvComment code more reenterable?
			return $retval;
		}
		$level += 1;

		$InstanceInd = $yvComment->BeginInstance('plugin', $params);

		$task = 'viewdisplay';

		// TODO: make code below the same for all calls of the controller
		// and move it inside the controller...
		$layoutName = $yvComment->getPageValue('layout_name', 'default');
		//$strOut .= ', $layoutName="' . $layoutName . '"';
		if ($layoutName == '0') {
			$layoutName = $yvComment->getPageValue('layout_name_custom', 'default');
		}
		JRequest::setVar('layout', $layoutName);

		//$strOut .= ', ArticleID=' . $ArticleID;
		$yvComment->setFilterByContext('article');
		$yvComment->setArticle($article);

		//$mainframe = JFactory::getApplication();
		//$message = print_r($yvComment->getArticleID(), true);
		//$mainframe->enqueueMessage($message, 'notice');

		if ($this->_PluginPlace != 'hide') {
			$config = array ();
			$config['task'] = $task;
			$config['view'] = $viewName;
			$config['comment_type_id'] = $this->CommentTypeId;
				
			// This is needed only because we can't 'undefine' this:
			//define( 'JPATH_COMPONENT',					JPATH_BASE.DS.'components'.DS.$name);
			$config['base_path'] = JPATH_SITE_YVCOMMENT;

			// Create the controller
			$controller = new yvcommentController($config);

			// Perform the Request task
			$controller->execute($task);

			$strOut .= $controller->getOutput();
		}

		$level -= 1;
		switch ($this->_PluginPlace) {
			case 'AfterContent' :
				$retval = $strOut;
				break;
			case 'DefinedByArticleTemplate' :
				// Article (blog, ...) template will put this
				// to the desired place
				$article->comments = $strOut;
				break;
			case 'InsideBox' :
				$article->text .= $strOut;
			case 'hide' :
			default :
		}
		$yvComment->EndInstance($InstanceInd);
		return $retval;
	}
}
/**
 * This class solves problem with Joomla! 1.6 Template override
 * I've looked into the JView code and found that
 *  it made one step back in support of MVC pattern for modules and plugins
 * Now single "component" is in fact hardcoded into the core...
 * (there is a call in 'libraries\joomla\application\component\view.php' to
 * ---
 * $component = JApplicationHelper::getComponentName()
 * ---
 * that can handle only ONE component per page  :-(
 */
class yvCommentJView extends JViewLegacy
{

	/**
	 * Sets an entire array of search paths for templates or resources.
	 *
	 * @access protected
	 * @param string 		The type of path to set, typically 'template'.
	 * @param string|array	The new set of search paths.  If null or false, resets to the current directory only.
	 */
	function _setPath($type, $path)
	{
		jimport('joomla.application.helper');

		// 2011-03-29 yvolk deleted this line and added one below it
		// $component	= JApplicationHelper::getComponentName();
		$component	= JRequest::getCmd('option');

		$app		= JFactory::getApplication();

		// clear out the prior search dirs
		$this->_path[$type] = array();

		// actually add the user-specified directories
		$this->_addPath($type, $path);

		// always add the fallback directories as last resort
		switch (strtolower($type))
		{
			case 'template':
				// Set the alternative template search dir
				if (isset($app))
				{
					$component	= preg_replace('/[^A-Z0-9_\.-]/i', '', $component);
					$fallback	= JPATH_THEMES.DS.$app->getTemplate().DS.'html'.DS.$component.DS.$this->getName();
					$this->_addPath('template', $fallback);
				}
				break;
		}
	}
}

/**
 * This is a Joomla! 1.7.0 bug workaround, 
 * see http://joomlacode.org/gf/project/joomla/tracker/?action=TrackerItemEdit&tracker_item_id=26596 
 *
 */
abstract class yvCommentJDatabase extends JDatabase {
	public static function replacePrefixStatic($db, $sql, $prefix='#__')
	{
		return $db->replacePrefix($sql, $prefix);
	}
}
?>
