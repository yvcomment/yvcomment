<?php
/**
 * @version		$Id: yvcommentspacer.php 22 2011-03-17 14:40:31Z yvolk $
 * @package		yvComment
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.form.formfield');

/**
 * Real need for this element is loading yvComment language file
 * To load all language strings, this 'parameter' should be
 *   the first in the list.
 */
class JFormFieldYvcommentspacer extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Yvcommentspacer';

	/**
	 * Method to instantiate the form field object.
	 *
	 * @param	object	$form	The form to attach to the form field object.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function __construct($form = null) {
		parent::__construct($form);

		// Let's do something custom...
		//$mainframe = JFactory::getApplication();
		//$message = 'Class initialized: ' . get_class($this);
		//$mainframe->enqueueMessage($message, 'notice');

		if (!class_exists('yvCommentHelper')) {
			$path = JPATH_SITE . DS . 'components' . DS . 'com_yvcomment' . DS . 'helpers.php';
			if (file_exists($path)) {
				require_once ($path);
			}
		}
		if (class_exists('yvCommentHelper')) {
			// Initialize yvComment, so Language file will be loaded...
			$yvComment = &yvCommentHelper::getInstance(1);
		}
	}

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getInput()
	{
		if (substr($this->fieldname,0,1) != '@') {
			return JText::_($this->fieldname) . '<hr />';
		} else {
			return '<hr />';
		}
	}
}